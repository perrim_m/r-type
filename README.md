# R-Type Project

### Updated 29/11/15

The project was part of the advanced C++ module in June 2014. For more information on the subject, a PDF `rtype.pdf` in the repository is available.
At the end, the project is functional and respect most requirements, but unfortunately the windows version of the game client can't be used, because resources are not loaded by the SDL (black screen).
However, this project has been done in one month. Plus, even with networks and synchronization problem, the project was marked: 16/20.

## Installation

### Build

	$ cmake .
	$ make install

### Clean

	$ make clean

## Usage

In the file `Rtype.conf` defines the IP address of the server which the client will communicate.
By default is on the localhost (127.0.0.1).

### Server
	$ ./Rtype_server
	
### Client
	$ ./Rtype_client
	
### How to play

After to launch the server and client(s) (If you have the message: "No conneciton to server", please check the IP address), create a room and join it. Other players will see the room, and can join.

#### Control

* Up: Up arrow key
* Down: Down arrow key
* Left: Left arrow key
* Right: Right arrow key
* Shoot: Space bar
* Quit: Escape key

## Pros and cons

### Pros

* Multi-platform (Mac, Linux, Windows)
* Entity Component System (ECS)
* Multi-player
* Multi-room

### Cons

* Network management not optimize (a thread for each connection instead of a "select")
* No game over
* No UI in game
//
// BulletBoss1.cpp for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Projectiles
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 20:23:58 2014 bizeul_f
// Last update Wed May  7 21:44:14 2014 bizeul_f
//


#include "BulletBoss1.hh"

BulletBoss1::BulletBoss1() : AEntity(60, 42, PROJECTILE, "BulletBoss1")
{
	this->_components[MOVE] = new MoveComponent(25, 0);
	this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/boss_1_projectile.png");
	this->_components[BULLET] = new BulletComponent(50, ENNEMY);
}

BulletBoss1::~BulletBoss1() {}

eEntityType	BulletBoss1::getType() const
{
	return (this->_type);
}

extern "C" AEntity	*getNewInstanceBulletBoss1()
{
	return (new BulletBoss1());
}

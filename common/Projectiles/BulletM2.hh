//
// BulletM2.hh for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Projectiles
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 21:32:20 2014 bizeul_f
// Last update Wed May  7 21:36:57 2014 bizeul_f
//
#ifndef BULLETM2_H_
# define BULLETM2_H_

# include <cstdlib>

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "AnimationComponent.hh"
# include "SimpleSpriteComponent.hh"
# include "BulletComponent.hh"

class BulletM2 : public AEntity
{
public:
	BulletM2();
	virtual ~BulletM2();

	eEntityType	getType() const;
};

#endif // !BULLETM2_HH_

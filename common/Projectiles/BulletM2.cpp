//
// BulletM2.cpp for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Projectiles
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 21:37:59 2014 bizeul_f
// Last update Wed May  7 21:40:03 2014 bizeul_f
//

#include "BulletM2.hh"

BulletM2::BulletM2() : AEntity(18, 16, PROJECTILE, "BulletM2")
{
	this->_components[MOVE] = new MoveComponent(-250, rand() % 100 * ((rand() % 2) ? -1 : 1));
	this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/bulletM2.png", 18, 16);
	this->_components[BULLET] = new BulletComponent(10, ENNEMY);
}

BulletM2::~BulletM2() {}

eEntityType	BulletM2::getType() const
{
	return (this->_type);
}

extern "C" AEntity	*getNewInstanceBulletM2()
{
	return (new BulletM2());
}

//
// BulletM1.cpp for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Projectiles
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 20:27:26 2014 bizeul_f
// Last update Wed May  7 20:30:18 2014 bizeul_f
//

#include "BulletM1.hh"

BulletM1::BulletM1() : AEntity(25, 25, PROJECTILE, "BulletM1")
{
	this->_components[MOVE] = new MoveComponent(15, 0);
	this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/monster1_projectile.png");
	this->_components[BULLET] = new BulletComponent(25, ENNEMY);
}

BulletM1::~BulletM1() {}

eEntityType	BulletM1::getType() const
{
	return (this->_type);
}

extern "C" AEntity	*getNewInstanceBulletM1()
{
	return (new BulletM1());
}

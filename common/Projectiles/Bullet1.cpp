//
// Bullet1.cpp for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Projectiles
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 21:40:42 2014 bizeul_f
// Last update Tue May 27 14:13:41 2014 bizeul_f
//

#include "Bullet1.hh"

Bullet1::Bullet1() : AEntity(17, 10, PROJECTILE, "Bullet1")
{
	this->_components[MOVE] = new MoveComponent(800, 0);
	this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/bullet_player.png", 17, 10);
	this->_components[BULLET] = new BulletComponent(25, FRIEND);
}

Bullet1::~Bullet1() {}

eEntityType	Bullet1::getType() const
{
	return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity	*getNewInstanceBullet1()
{
	return (new Bullet1());
}

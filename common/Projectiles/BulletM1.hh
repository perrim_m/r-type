//
// BulletM1.hh for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Projectiles
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 21:30:02 2014 bizeul_f
// Last update Wed May  7 21:30:29 2014 bizeul_f
//

#ifndef BULLETM1_HH_
# define BULLETM1_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "AnimationComponent.hh"
# include "SimpleSpriteComponent.hh"
# include "BulletComponent.hh"

class BulletM1 : public AEntity
{
public:
	BulletM1();
	virtual ~BulletM1();

	eEntityType	getType() const;
};

#endif // !BULLETM1_HH_

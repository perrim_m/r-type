//
// Bullet1.hh for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Projectiles
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 21:43:50 2014 bizeul_f
// Last update Wed May  7 21:43:57 2014 bizeul_f
//


#ifndef BULLET_HH_
# define BULLET_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "AnimationComponent.hh"
# include "SimpleSpriteComponent.hh"
# include "BulletComponent.hh"

class Bullet1 : public AEntity
{
public:
	Bullet1();
	virtual ~Bullet1();

	eEntityType	getType() const;
};

#endif // !BULLET_HH_

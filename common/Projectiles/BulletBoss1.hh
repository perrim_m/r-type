//
// BulletBoss1.hh for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Projectiles
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 20:26:19 2014 bizeul_f
// Last update Wed May  7 20:27:11 2014 bizeul_f
//

#ifndef BULLETBOSS1_HH_
# define BULLETBOSS1_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "AnimationComponent.hh"
# include "SimpleSpriteComponent.hh"
# include "BulletComponent.hh"

class BulletBoss1 : public AEntity
{
public:
	BulletBoss1();
	virtual ~BulletBoss1();

	eEntityType	getType() const;
};

#endif // !BULLETBOSS1_HH_

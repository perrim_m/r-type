//
//  BulletComponent.hh
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef BULLETCOMPONENT_HH_
# define BULLETCOMPONENT_HH_

# include "IComponent.hh"
# include "AEntity.hh"

enum eOwner
{
	FRIEND,
	ENNEMY,
};

class BulletComponent : public IComponent
{
private:
	eOwner	_owner;
	int		_damage;

public:
	BulletComponent(int, eOwner);
	virtual ~BulletComponent();

	bool	update(AEntity *, float);
	eOwner	getOwner() const;
	int		getDamage() const;
};

#endif // !BULLETCOMPONENT_HH_

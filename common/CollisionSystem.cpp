#include <iostream>
#include "CollisionSystem.hh"

CollisionSystem::CollisionSystem(SerialisationServer *ss, SafeQueue<std::string> *toSend)
{
	this->_ss = ss;
	this->_toSend = toSend;
}

CollisionSystem::~CollisionSystem()
{
}

std::list<AEntity *>	CollisionSystem::lookForCollisions(const std::list<AEntity *>& entities, std::list<AEntity *> list) const
{
	std::list<AEntity *>::const_iterator	it;
	std::list<AEntity *>::const_iterator	it2;
	
	it = entities.begin();
	while (it != entities.end())
    {
		it2 = entities.begin();
		while (it2 != entities.end())
		{
			if (*it != *it2)
			{
				if (((*it)->getType() == PLAYER && ((*it2)->getType() == PLAYER || ((*it2)->getType() == PROJECTILE && reinterpret_cast<BulletComponent *>((*it2)->getComponent(BULLET))->getOwner() == FRIEND))) ||
					((*it2)->getType() == PLAYER && ((*it)->getType() == PLAYER || ((*it)->getType() == PROJECTILE && reinterpret_cast<BulletComponent *>((*it)->getComponent(BULLET))->getOwner() == FRIEND))) ||
					((*it2)->getType() == PROJECTILE && (*it)->getType() == PROJECTILE) ||
					((*it)->getType() == MONSTER && ((*it2)->getType() == MONSTER || ((*it2)->getType() == PROJECTILE && reinterpret_cast<BulletComponent *>((*it2)->getComponent(BULLET))->getOwner() == ENNEMY))) ||
					((*it2)->getType() == MONSTER && ((*it)->getType() == MONSTER || ((*it)->getType() == PROJECTILE && reinterpret_cast<BulletComponent *>((*it)->getComponent(BULLET))->getOwner() == ENNEMY))) ||
					(*it)->getX() > 800 || (*it2)->getX() > 800)
				{
					++it2;
					continue;
				}
				else if ((*it)->getX() < (*it2)->getX() &&
						 (*it)->getX() + (*it)->getWidth() > (*it2)->getX())
				{
					if ((*it)->getY() < (*it2)->getY() &&
						(*it)->getY() + (*it)->getHeight() > (*it2)->getY())
					{
						if (!this->itExistsInList(list, it))
							list.push_back(*it);
						if (!this->itExistsInList(list, it2))
							list.push_back(*it2);
					}
					else if ((*it)->getY() > (*it2)->getY() &&
							 (*it)->getY() < (*it2)->getY() + (*it2)->getHeight())
					{
						if (!this->itExistsInList(list, it))
							list.push_back(*it);
						if (!this->itExistsInList(list, it2))
							list.push_back(*it2);
					}
				}
				else if ((*it)->getX() > (*it2)->getX() &&
						 (*it)->getX() < (*it2)->getX() + (*it2)->getWidth())
				{
					if ((*it)->getY() < (*it2)->getY() &&
						(*it)->getY() + (*it)->getHeight() > (*it2)->getY())
					{
						if (!this->itExistsInList(list, it))
							list.push_back(*it);
						if (!this->itExistsInList(list, it2))
							list.push_back(*it2);
					}
					else if ((*it)->getY() > (*it2)->getY() &&
							 (*it)->getY() < (*it2)->getY() + (*it2)->getHeight())
					{
						if (!this->itExistsInList(list, it))
							list.push_back(*it);
						if (!this->itExistsInList(list, it2))
							list.push_back(*it2);
					}
				}
			}
			++it2;
		}
		++it;
    }
	return (list);
}

void	CollisionSystem::quadTree(std::list<AEntity *>& touchedEntities,
								  std::list<AEntity *>& entities,
								  int deep, int x1, int y1, int sizeX, int sizeY) const
{
	std::list<AEntity *>			list1;
	std::list<AEntity *>			list2;
	std::list<AEntity *>			list3;
	std::list<AEntity *>			list4;
	std::list<AEntity *>::const_iterator	it;
	AEntity				*entity;
	int					x2;
	int					x3;
	int					x4;
	int					y2;
	int					y3;
	int					y4;
	
	if (deep > 2 || entities.size() < 4)
    {
		touchedEntities = this->lookForCollisions(entities, touchedEntities);
		return ;
    }
	x2 = x1 + sizeX;
	x3 = x1;
	x4 = x3;
	y2 = y1;
	y3 = y1 + sizeY;
	y4 = y3;
	sizeX /= 2;
	sizeY /= 2;
	while ((it = entities.begin()) != entities.end())
    {
		entity = *it;
		if (entity->getX() <= x2)
		{
			if (entity->getY() <= y3)
			{
				list1.push_back(entity);
			}
			else
			{
				list3.push_back(entity);
			}
		}
		else
		{
			if (entity->getY() <= y3)
			{
				list2.push_back(entity);
			}
			else
			{
				list4.push_back(entity);
			}
		}
		++it;
    }
	this->quadTree(touchedEntities, list1, deep + 1, x1, y1, sizeX, sizeY);
	this->quadTree(touchedEntities, list2, deep + 1, x2, y2, sizeX, sizeY);
	this->quadTree(touchedEntities, list3, deep + 1, x3, y3, sizeX, sizeY);
	this->quadTree(touchedEntities, list4, deep + 1, x4, y4, sizeX, sizeY);
}

void	CollisionSystem::update(std::list<AEntity *> &entities, float)
{
	std::list<AEntity *>			nonTouchedEntities(entities);
	std::list<AEntity *>			touchedEntities;
	std::list<AEntity *>::iterator	it;
	HealthComponent			*health;
	
	this->quadTree(touchedEntities, nonTouchedEntities, 3, 0, 0, 1000, 600);
	
	if (touchedEntities.size() > 0)
    {
		it = touchedEntities.begin();
		while (it != touchedEntities.end())
		{
			if ((*it)->getType() == MONSTER || (*it)->getType() == PLAYER)
			{
				if ((health = reinterpret_cast<HealthComponent *>((*it)->getComponent(HEALTH))) != NULL)
				{
					std::cout << "Health : " << health->getHp() << std::endl;
					if (health->takeDamage(1) == false)
					{
						entities.remove(*it);
						this->_toSend->lock();
						this->_toSend->push(this->_ss->removeEntity(*it));
						this->_toSend->unlock();
						delete *it;
						*it = NULL;
					}
				}
			}
			else
			{
				entities.remove(*it);
				this->_toSend->lock();
				this->_toSend->push(this->_ss->removeEntity(*it));
				this->_toSend->unlock();
				delete *it;
				*it = NULL;
			}
			it = touchedEntities.erase(it);
		}
    }
}

bool	CollisionSystem::itExistsInList(const std::list<AEntity *>& myList,
										const std::list<AEntity *>::const_iterator& it) const
{
	std::list<AEntity *>::const_iterator	it2 = myList.begin();
	
	while (it2 != myList.end())
    {
		if (*it2 == *it)
			return (true);
		++it2;
    }
	return (false);
}

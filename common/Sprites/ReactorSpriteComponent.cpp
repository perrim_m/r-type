//
//  ReactorSpriteComponent.cpp
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#include "ReactorSpriteComponent.hh"

ReactorSpriteComponent::ReactorSpriteComponent() : SpriteComponent("./sprites/reactor.png", 66.5, 0, 33.25, 34, 0.8f)
{
	this->_frameCount = 0;
}

ReactorSpriteComponent::~ReactorSpriteComponent()
{
}

bool	ReactorSpriteComponent::update(AEntity *entity, float frameRate)
{
	MoveComponent			*mc;

	if ((mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE))) != NULL)
	{
		this->_frameCount += frameRate;
		if (mc->getDX() < 0)
		{
			this->_x = 99.75;
		}
		else if (mc->getDX() > 0)
		{
			this->_x = 0;
		}
		else
		{
			if (this->_frameCount > 0.2)
			{
				this->_x = (this->_x == 33.25) ? 66.5 : 33.25;
				this->_frameCount = 0;
			}
		}
	}
	return (true);
}
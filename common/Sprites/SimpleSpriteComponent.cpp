//
//  SimpleSpriteComponent.cpp
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#include "SimpleSpriteComponent.hh"

SimpleSpriteComponent::SimpleSpriteComponent(const std::string &file, float width, float height) : SpriteComponent(file, 0, 0, width, height, 1)
{
}

SimpleSpriteComponent::SimpleSpriteComponent(const std::string &file, float width, float height, float scale) : SpriteComponent(file, 0, 0, width, height, scale)
{
}

SimpleSpriteComponent::~SimpleSpriteComponent()
{
}

bool	SimpleSpriteComponent::update(AEntity *, float)
{
	return (true);
}
//
//  ReactorSpriteComponent.h
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#ifndef REACTORSPRITECOMPONENT_HH_
# define REACTORSPRITECOMPONENT_HH_

# include "SpriteComponent.hh"
# include "MoveComponent.hh"

class ReactorSpriteComponent : public SpriteComponent
{
private:
	float	_frameCount;

public:
	ReactorSpriteComponent();
	virtual ~ReactorSpriteComponent();

	bool	update(AEntity *, float);
};

#endif // !REACTORSPRITECOMPONENT_HH_

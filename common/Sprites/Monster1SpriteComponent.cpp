//
//  Monster1SpriteComponent.cpp
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#include "Monster1SpriteComponent.hh"

Monster1SpriteComponent::Monster1SpriteComponent() :
  SpriteComponent("./sprites/monster1.png", 234.5f, 5, 33.5, 36, 1.5f)
{
	this->_frameCount = 0;
}

Monster1SpriteComponent::~Monster1SpriteComponent()
{
}

bool	Monster1SpriteComponent::update(AEntity *, float frameRate)
{
	this->_frameCount += frameRate;
	if (this->_frameCount > 0.2)
	{
		this->_frameCount = 0;
		if (this->_x == 0)
			this->_x = 234.5f;
		else
			this->_x -= this->_width;
	}
	return (true);
}

//
//  Monster1SpriteComponent.h
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#ifndef MONSTER1SPRITECOMPONENT_HH_
# define MONSTER1SPRITECOMPONENT_HH_

# include "SpriteComponent.hh"

class Monster1SpriteComponent : public SpriteComponent
{
private:
	float	_frameCount;

public:
	Monster1SpriteComponent();
	virtual ~Monster1SpriteComponent();
	
	bool	update(AEntity *, float);
};

#endif // !MONSTER1SPRITECOMPONENT_HH_

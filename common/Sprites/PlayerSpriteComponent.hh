//
//  PlayerSpriteComponent.h
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#ifndef PLAYERSPRITECOMPONENT_HH_
# define PLAYERSPRITECOMPONENT_HH_

# include <string>

# include "SpriteComponent.hh"
# include "MoveComponent.hh"

class PlayerSpriteComponent : public SpriteComponent
{
private:
	float	_frameCount;
	
public:
	PlayerSpriteComponent(const std::string &);
	virtual ~PlayerSpriteComponent();
	
	bool	update(AEntity *, float);
};

#endif // !PLAYERSPRITECOMPONENT_HH_

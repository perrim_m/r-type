//
//  SimpleSpriteComponent.h
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef SIMPLESPRITECOMPONENT_HH_
# define SIMPLESPRITECOMPONENT_HH_

# include <string>

# include "SpriteComponent.hh"

class SimpleSpriteComponent : public SpriteComponent
{
public:
	SimpleSpriteComponent(const std::string &, float, float);
	SimpleSpriteComponent(const std::string &, float, float, float);
	virtual ~SimpleSpriteComponent();

	bool	update(AEntity *, float);
};

#endif // !SIMPLESPRITECOMPONENT_HH_

//
//  PlayerSpriteComponent.cpp
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#include "PlayerSpriteComponent.hh"

PlayerSpriteComponent::PlayerSpriteComponent(const std::string &file) : SpriteComponent(file, 66, 0, 33, 16, 1.5f)
{
	this->_frameCount = 0;
}

PlayerSpriteComponent::~PlayerSpriteComponent()
{
}

bool						PlayerSpriteComponent::update(AEntity *entity, float frameRate)
{
	MoveComponent			*mc;

	if ((mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE))) != NULL)
	{
		this->_frameCount += frameRate;
		
		if (this->_frameCount > 0.5)
		{
			if (mc->getDY() > 0 && this->_x > 0)
			{
				this->_x -= 33;
			}
			else if (mc ->getDY() < 0 && this->_x < 132)
			{
				this->_x += 33;
			}
			else
			{
				if (this->_x < 66)
					this->_x += 33;
				else if (this->_x > 66)
					this->_x -= 33;
			}
			this->_frameCount = 0;
		}
		
	}
	return (true);
}
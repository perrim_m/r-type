//
//  ExplosionSpriteComponent.cpp
//  R-Type
//
//  Created by Stern on 01/06/2014.
//
//

#include "ExplosionSpriteComponent.hh"

ExplosionSpriteComponent::ExplosionSpriteComponent() : SpriteComponent("./sprites/r-typesheet44.gif", 0, 100, 50, 50, 1.0f)
{
	this->_frameCount = 0.0f;
	this->_frameMax = 1;
}

ExplosionSpriteComponent::~ExplosionSpriteComponent()
{
}

bool	ExplosionSpriteComponent::update(AEntity *, float frameRate)
{
	this->_frameCount += frameRate;
	this->_frameInterval += frameRate;
	if (this->_frameCount >= this->_frameMax)
		return (false);
	if (this->_frameInterval >= 0.20)
	{
		this->_x += 50;
	}
	return (true);
}
#ifndef MONSTER2SPRITECOMPONENT_HH_
# define MONSTER2SPRITECOMPONENT_HH_

# include "SpriteComponent.hh"

class Monster2SpriteComponent : public SpriteComponent
{
private:
  float	_frameCount;

public:
  Monster2SpriteComponent();
  virtual ~Monster2SpriteComponent();

  bool	update(AEntity *, float);
};

#endif // !MONSTER2SPRITECOMPONENT_HH_

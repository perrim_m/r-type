#include "Boss1SpriteComponent.hh"

Boss1SpriteComponent::Boss1SpriteComponent() :
  SpriteComponent("./sprites/boss1.png", 117.5f, 0, 67.0f, 81, 1.5f)
{
  this->_frameCount = 0;
}

Boss1SpriteComponent::~Boss1SpriteComponent()
{
}

bool	Boss1SpriteComponent::update(AEntity *, float frameRate)
{
  this->_frameCount += frameRate;
  if (this->_frameCount > 0.2)
    {
      this->_frameCount = 0;
      if (this->_x <= 184.5f)
	this->_x = 385.5f;
      else
	this->_x -= this->_width;
    }
  return (true);
}

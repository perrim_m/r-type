#include "Monster2SpriteComponent.hh"

Monster2SpriteComponent::Monster2SpriteComponent() :
  SpriteComponent("./sprites/monster2.png", 0.0f, 0, 33.4, 34, 1.0f)
{
  this->_frameCount = 0;
}

Monster2SpriteComponent::~Monster2SpriteComponent()
{
}

bool	Monster2SpriteComponent::update(AEntity *, float frameRate)
{
  this->_frameCount += frameRate;
  if (this->_frameCount > 0.2)
    {
      this->_frameCount = 0;
      if (this->_x < 0)
	this->_x = 66.6f;
      else
	this->_x -= this->_width;
    }
  return (true);
}

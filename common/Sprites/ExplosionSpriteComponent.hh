//
//  ExplosionSpriteComponent.h
//  R-Type
//
//  Created by Stern on 01/06/2014.
//
//

#ifndef EXPLOSIONSPRITECOMPONENT_HH_
# define EXPLOSIONSPRITECOMPONENT_HH_

# include "SpriteComponent.hh"

class ExplosionSpriteComponent : public SpriteComponent
{
private:
	float		_frameCount;
	float		_frameMax;
	float		_frameInterval;
	
public:
	ExplosionSpriteComponent();
	virtual ~ExplosionSpriteComponent();
	
	bool	update(AEntity *, float);
};

#endif // !EXPLOSIONSPRITECOMPONENT_HH_

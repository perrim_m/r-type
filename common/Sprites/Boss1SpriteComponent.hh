#ifndef BOSS1SPRITECOMPONENT_HH_
# define BOSS1SPRITECOMPONENT_HH_

# include "SpriteComponent.hh"

class Boss1SpriteComponent : public SpriteComponent
{
private:
	float	_frameCount;

public:
	Boss1SpriteComponent();
	virtual ~Boss1SpriteComponent();
	
	bool	update(AEntity *, float);
};

#endif // !BOSS1SPRITECOMPONENT_HH_

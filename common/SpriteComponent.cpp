//
//  SpriteComponent.cpp
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#include "SpriteComponent.hh"

SpriteComponent::SpriteComponent(const std::string &sprite, float x, float y, float width, float height, float scale)
{
	this->_sprite = sprite;
	this->_x = x;
	this->_y = y;
	this->_width = width;
	this->_height = height;
	this->_scale = scale;
}

SpriteComponent::~SpriteComponent()
{
}

const std::string	&SpriteComponent::getSprite() const
{
	return (this->_sprite);
}

float	SpriteComponent::getX() const
{
	return (this->_x);
}

float	SpriteComponent::getY() const
{
	return (this->_y);
}

float	SpriteComponent::getWidth() const
{
	return (this->_width);
}

float	SpriteComponent::getHeight() const
{
	return (this->_height);
}

float	SpriteComponent::getScale() const
{
	return (this->_scale);
}
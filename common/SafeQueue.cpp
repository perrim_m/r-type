//
//  SafeQueue.cpp
//  Plazza
//
//  Created by Stern on 26/04/2014.
//
//

#include "SafeQueue.hh"

template <class T>
SafeQueue<T>::SafeQueue(IMutex *mutex)
{
	this->_mutex = mutex;
}

template <class T>
SafeQueue<T>::~SafeQueue()
{
	delete this->_mutex;
}

template <class T>
void	SafeQueue<T>::push(T elem)
{
	this->_queue.push(elem);
}

template <class T>
T	SafeQueue<T>::get()
{
	T	elem = this->_queue.front();
	this->_queue.pop();
	return (elem);
}

template <class T>
bool	SafeQueue<T>::isEmpty() const
{
	return (this->_queue.empty());
}

template <class T>
size_t	SafeQueue<T>::size() const
{
	return (this->_queue.size());
}

template <class T>
void	SafeQueue<T>::clear()
{
	while (!this->_queue.empty())
	{
		this->_queue.pop();
	}
}

template <class T>
void	SafeQueue<T>::lock()
{
	this->_mutex->lock();
}

template <class T>
void	SafeQueue<T>::unlock()
{
	this->_mutex->unlock();
}

template   				SafeQueue<std::string>::SafeQueue(IMutex *);
template      			SafeQueue<std::string>::~SafeQueue();
template void	       	SafeQueue<std::string>::push(std::string);
template std::string	SafeQueue<std::string>::get();
template bool  			SafeQueue<std::string>::isEmpty() const;
template size_t	       	SafeQueue<std::string>::size() const;
template void			SafeQueue<std::string>::clear();
template void  			SafeQueue<std::string>::lock();
template void  			SafeQueue<std::string>::unlock();

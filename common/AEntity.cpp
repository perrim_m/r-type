//
//  AEntity.cpp
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#include "AEntity.hh"

AEntity::AEntity(float width, float height, eEntityType type, const std::string &name) : _type(type), _name(name)
{
	this->_id = -1;
	this->_x = 0;
	this->_y = 0;
	this->_width = width;
	this->_height = height;
}

AEntity::AEntity(const AEntity &other) : _type(other._type)
{
	this->_id = -1;
	this->_x = other._x;
	this->_y = other._y;
	this->_width = other._width;
	this->_height = other._height;
	this->_components = other._components;
}

AEntity::~AEntity()
{
	std::map<eComponent, IComponent *>::iterator it;

	while (!this->_components.empty())
	{
		it = this->_components.begin();
		delete it->second;
		this->_components.erase(it);
	}
}

bool	AEntity::updateComponent(eComponent ec, float frameRate)
{
	if (this->_components.find(ec) != this->_components.end())
	{
		return (this->_components[ec]->update(this, frameRate));
	}
	return (false);
}

IComponent	*AEntity::getComponent(eComponent ec) const
{
	return ((this->_components.find(ec) != this->_components.end()) ? this->_components.at(ec) : NULL);
}

int	AEntity::getId() const
{
	return (this->_id);
}

float	AEntity::getX() const
{
	return (this->_x);
}

float	AEntity::getY() const
{
	return (this->_y);
}

float	AEntity::getWidth() const
{
	return (this->_width);
}

float	AEntity::getHeight() const
{
	return (this->_height);
}

const std::string	&AEntity::getName() const
{
	return (this->_name);
}

void	AEntity::setId(int id)
{
	this->_id = id;
}

void	AEntity::setX(float x)
{
	this->_x = x;
}

void	AEntity::setY(float y)
{
	this->_y = y;
}

void	AEntity::setWidth(float width)
{
	this->_width = width;
}

void	AEntity::setHeight(float height)
{
	this->_height = height;
}
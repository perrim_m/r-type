//
//  HealthComponent.h
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef HEALTHCOMPONENT_HH_
# define HEALTHCOMPONENT_HH_

# include "IComponent.hh"
# include "AEntity.hh"

class HealthComponent : public IComponent
{
private:
	int	_lives;
	int	_hp;

public:
	HealthComponent(int, int);
	virtual ~HealthComponent();

	bool	update(AEntity *, float);
	int		getLives() const;
	int		getHp() const;
	bool	takeDamage(int);
};

#endif // !HEALTHCOMPONENT_HH_

//
//  AnimationComponent.h
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef ANIMATIONCOMPONENT_HH_
# define ANIMATIONCOMPONENT_HH_

# include "IComponent.hh"
# include "AEntity.hh"

class AnimationComponent : public IComponent
{
public:
	AnimationComponent();
	virtual ~AnimationComponent();

	virtual bool	update(AEntity *, float) = 0;
};

#endif // !ANIMATIONCOMPONENT_HH_

//
//  InputComponent.cpp
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#include "InputComponent.hh"

InputComponent::InputComponent()
{
	this->_speed = 400;
}

InputComponent::~InputComponent()
{
}

bool				InputComponent::update(AEntity *, float)
{
	return (true);
}

void	InputComponent::shoot(AEntity *entity)
{
	WeaponComponent *wc;

	if ((wc = reinterpret_cast<WeaponComponent *>(entity->getComponent(WEAPON))) == NULL)
		throw std::runtime_error("");
	wc->shoot();
}

void	InputComponent::moveStop(AEntity *entity)
{
	MoveComponent	*mc;
	
	if ((mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE))) == NULL)
		throw std::runtime_error("");
	mc->setDY(0);
	mc->setDX(0);
}

void	InputComponent::moveUp(AEntity *entity)
{
	MoveComponent	*mc;

	if ((mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE))) == NULL)
		throw std::runtime_error("");
	mc->setDY(-this->_speed);
}


void	InputComponent::moveDown(AEntity *entity)
{
	MoveComponent	*mc;
	
	if ((mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE))) == NULL)
		throw std::runtime_error("");
	mc->setDY(this->_speed);
}

void	InputComponent::moveRight(AEntity *entity)
{
	MoveComponent	*mc;
	
	if ((mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE))) == NULL)
		throw std::runtime_error("");
	mc->setDX(this->_speed);
}

void	InputComponent::moveLeft(AEntity *entity)
{
	MoveComponent	*mc;
	
	if ((mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE))) == NULL)
		throw std::runtime_error("");
	mc->setDX(-this->_speed);
}
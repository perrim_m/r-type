//
//  FrameControl.hh
//  R-Type
//
//  Created by Stern on 26/05/2014.
//
//

#ifndef FRAMECONTROL_HH_
# define FRAMECONTROL_HH_

# ifdef _WIN32
#  include <Windows.h>
#endif

# include <ctime>
# include <cstring>

class FrameControl
{
private:
	clock_t	_timeLast;
	float	_frame;
	float	_frameRate;
	float	_interval;

public:
	FrameControl(float);
	virtual ~FrameControl();

	void	wait();
	bool	atInterval(float);
	float	getFrame() const;
	float	getFrameRate() const;
};

#endif // !FRAMECONTROL_HH_

//
//  Monster2WeaponComponent.h
//  R-Type
//
//  Created by Stern on 01/06/2014.
//
//

#ifndef MONSTER2WEAPONCOMPONENT_HH_
# define MONSTER2WEAPONCOMPONENT_HH_

# include "WeaponComponent.hh"

class Monster2WeaponComponent : public WeaponComponent
{
private:
	const float	_limitFire;
	float		_frameCount;
	
public:
	Monster2WeaponComponent();
	virtual ~Monster2WeaponComponent();

	bool	update(AEntity *, float);
};

#endif // !MONSTER2WEAPONCOMPONENT_HH_

//
//  PlayerWeapon1Component.h
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef PLAYERWEAPONCOMPONENT_HH_
# define PLAYERWEAPONCOMPONENT_HH_

# include <map>

# include "WeaponComponent.hh"

class PlayerWeaponComponent : public WeaponComponent
{
private:
	const float	_limitFire;
	
public:
	PlayerWeaponComponent();
	virtual ~PlayerWeaponComponent();
	
	bool	update(AEntity *, float);
};

#endif // !PLAYERWEAPONCOMPONENT_HH_

//
//  PlayerWeaponComponent.cpp
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#include "PlayerWeaponComponent.hh"

PlayerWeaponComponent::PlayerWeaponComponent() : WeaponComponent("Bullet1"), _limitFire(0.2)
{
}

PlayerWeaponComponent::~PlayerWeaponComponent()
{
}

bool	PlayerWeaponComponent::update(AEntity *entity, float frameRate)
{
	static std::map<int, float>	frameCounts;

	if (frameCounts.find(entity->getId()) == frameCounts.end())
		frameCounts[entity->getId()] = 0.0f;

	frameCounts[entity->getId()] += frameRate;
	if (this->_shoot)
	{
		if (frameCounts[entity->getId()] > this->_limitFire)
		{
			frameCounts[entity->getId()] = 0;
			this->_shoot = false;
			return (true);
		}
	}
	return (false);
}

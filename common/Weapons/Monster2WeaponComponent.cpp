//
//  Monster2WeaponComponent.cpp
//  R-Type
//
//  Created by Stern on 01/06/2014.
//
//

#include "Monster2WeaponComponent.hh"

Monster2WeaponComponent::Monster2WeaponComponent() : WeaponComponent("BulletM2"), _limitFire(0.5)
{
	this->_frameCount = 0.0f;
}

Monster2WeaponComponent::~Monster2WeaponComponent()
{
}

bool	Monster2WeaponComponent::update(AEntity *entity, float frameRate)
{
	this->_frameCount += frameRate;
	if (this->_frameCount >= this->_limitFire)
	{
		this->_frameCount -= this->_limitFire;
		return (true);
	}
	return (false);
}
//
//  EffectComponent.cpp
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#include "ReactorComponent.hh"

ReactorComponent::ReactorComponent(SpriteComponent *sprite)
{
	this->_sprite = sprite;
}

ReactorComponent::~ReactorComponent()
{
	delete this->_sprite;
}

bool	ReactorComponent::update(AEntity *entity, float frameRate)
{	
	this->_posX = entity->getX() - (this->_sprite->getWidth() * this->_sprite->getScale());
	this->_posY = entity->getY() + (entity->getHeight() / 2) - ((this->_sprite->getHeight() / 2) * this->_sprite->getScale());
	this->_sprite->update(entity, frameRate);
	return (true);
}

SpriteComponent	*ReactorComponent::getSprite() const
{
	return (this->_sprite);
}

float	ReactorComponent::getPosX() const
{
	return (this->_posX);
}

float	ReactorComponent::getPosY() const
{
	return (this->_posY);
}

//
// OmbrePlanet.cpp for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sun Jun  1 17:17:41 2014 bizeul_f
// Last update Sun Jun  1 17:36:12 2014 bizeul_f
//

#include "OmbrePlanet.hh"

OmbrePlanet::OmbrePlanet() : AEntity (795, 207, BACKGROUND, "OmbrePlanet")
{
  this->_components[MOVE] = new MoveComponent(-2, 0);
  this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/terre_ombre.png", 795, 207);
}

OmbrePlanet::~OmbrePlanet() {}

eEntityType OmbrePlanet::getType() const
{
  return (this->_type);
}

extern "C" AEntity *getNewInstanceOmbrePlanet()
{
  return (new OmbrePlanet());
}

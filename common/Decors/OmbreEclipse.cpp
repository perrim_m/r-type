//
// OmbreEclipse.cpp for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sat May 31 22:41:26 2014 bizeul_f
// Last update Sat May 31 23:37:13 2014 bizeul_f
//


#include "OmbreEclipse.hh"

OmbreEclipse::OmbreEclipse() : AEntity (350, 320, BACKGROUND, "OmbreEclipse")
{
  this->_components[MOVE] = new MoveComponent(-0.75, 0);
  this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/ombreeclipse.png", 350, 320);
}

OmbreEclipse::~OmbreEclipse() {}

eEntityType OmbreEclipse::getType() const
{
  return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity *getNewInstanceOmbreEclipse()
{
  return (new OmbreEclipse());
}

//
// Planet.cpp for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sat May 31 19:02:29 2014 bizeul_f
// Last update Sun Jun  1 17:32:06 2014 bizeul_f
//

#include "Planet.hh"

Planet::Planet() : AEntity (800, 210, BACKGROUND, "Planet")
{
  this->_components[MOVE] = new MoveComponent(0, 0);
  this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/planet.png", 800, 600);
}

Planet::~Planet() {}

eEntityType Planet::getType() const
{
  return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity *getNewInstancePlanet()
{
  return (new Planet());
}

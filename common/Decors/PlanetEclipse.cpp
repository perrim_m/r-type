//
// PlanetEclipse.cpp for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sat May 31 22:45:12 2014 bizeul_f
// Last update Sat May 31 23:37:55 2014 bizeul_f
//

#include "PlanetEclipse.hh"

PlanetEclipse::PlanetEclipse() : AEntity (350, 350, BACKGROUND, "PlanetEclise")
{
  this->_components[MOVE] = new MoveComponent(-0, 0);
  this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/planeteclipse.png", 350, 320);
}

PlanetEclipse::~PlanetEclipse() {}

eEntityType PlanetEclipse::getType() const
{
  return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity *getNewInstancePlanetEclipse()
{
  return (new PlanetEclipse());
}

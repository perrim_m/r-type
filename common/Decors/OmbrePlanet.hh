//
// OmbrePlanet.hh for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sun Jun  1 17:16:36 2014 bizeul_f
// Last update Sun Jun  1 17:17:31 2014 bizeul_f
//

#ifndef OMBREPLANET_HH_
# define OMBREPLANET_HH_

# include <cstdlib>

# include "AEntity.hh"
# include "SimpleSpriteComponent.hh"
# include "MoveComponent.hh"

class OmbrePlanet : public AEntity
{
public:
  OmbrePlanet();
  ~OmbrePlanet();

  eEntityType	getType() const;
};

#endif /* !OMBREPLANET_HH_ */

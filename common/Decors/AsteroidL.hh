//
// AsteroidL.cpp for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sat May 31 17:02:48 2014 bizeul_f
// Last update Sat May 31 18:46:59 2014 bizeul_f
//

#ifndef ASTEROIDL_HH_
# define ASTEROIDL_HH_

# include <cstdlib>

# include "AEntity.hh"
# include "SimpleSpriteComponent.hh"
# include "MoveComponent.hh"

class AsteroidL : public AEntity
{
public:
  AsteroidL();
  ~AsteroidL();

  eEntityType	getType() const;
};

#endif /* !ASTEROIDL_HH_ */

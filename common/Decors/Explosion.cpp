//
//  Explosion.cpp
//  R-Type
//
//  Created by Stern on 01/06/2014.
//
//

#include "Explosion.hh"

Explosion::Explosion() : AEntity(45, 45, BACKGROUND, "Explosion")
{
	this->_components[MOVE] = new MoveComponent();
	
}

Explosion::~Explosion()
{
}

eEntityType	Explosion::getType() const
{
	return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity *getNewInstanceExplosion()
{
	return (new Explosion());
}
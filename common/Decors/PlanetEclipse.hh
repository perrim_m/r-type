//
// PlanetEclipse.hh for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sat May 31 22:44:03 2014 bizeul_f
// Last update Sat May 31 22:44:55 2014 bizeul_f
//

#ifndef PLANETECLIPSE_HH_
# define PLANETECLIPSE_HH_

# include <cstdlib>

# include "AEntity.hh"
# include "SimpleSpriteComponent.hh"
# include "MoveComponent.hh"

class PlanetEclipse : public AEntity
{
public:
  PlanetEclipse();
  ~PlanetEclipse();

  eEntityType	getType() const;
};

#endif /* !PLANETECLIPSE_HH_ */

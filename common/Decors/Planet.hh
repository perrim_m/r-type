//
// Planet.hh for R-Type in /home/bizeul_f/Developpement/r-type
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sun Jun  1 16:31:10 2014 bizeul_f
// Last update Sun Jun  1 16:32:03 2014 bizeul_f
//

#ifndef PLANET_HH_
# define PLANET_HH_

# include <cstdlib>

# include "AEntity.hh"
# include "SimpleSpriteComponent.hh"
# include "MoveComponent.hh"

class Planet : public AEntity
{
  public:
  Planet();
  ~Planet();

  eEntityType   getType() const;
};

#endif /* !PLANET_HH_ */

//
//  Explosion.h
//  R-Type
//
//  Created by Stern on 01/06/2014.
//
//

#ifndef EXPLOSION_HH_
# define EXPLOSION_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include ""

class Explosion : public AEntity
{
public:
	Explosion();
	virtual ~Explosion();

	eEntityType	getType() const;
};

#endif // !EXPLOSION_HH_

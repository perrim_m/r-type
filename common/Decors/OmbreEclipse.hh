//
// OmbreEclipse.hh for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sat May 31 22:39:34 2014 bizeul_f
// Last update Sat May 31 22:40:27 2014 bizeul_f
//

#ifndef OMBREECLIPSE_HH_
# define OMBREECLIPSE_HH_

# include <cstdlib>

# include "AEntity.hh"
# include "SimpleSpriteComponent.hh"
# include "MoveComponent.hh"

class OmbreEclipse : public AEntity
{
public:
  OmbreEclipse();
  ~OmbreEclipse();

  eEntityType	getType() const;
};

#endif /* !OMBREECLIPSE_HH_ */

//
// AsteroidS.cpp for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sat May 31 17:02:48 2014 bizeul_f
// Last update Sun Jun  1 15:26:44 2014 bizeul_f
//

#include "AsteroidS.hh"

AsteroidS::AsteroidS() : AEntity(32, 32, BACKGROUND, "AsteroidS")
{
  this->_components[MOVE] = new MoveComponent((rand() % 75) * -1, rand() % 20 * ((rand() % 2) ? -1 : 1));
  this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/asteroidS.png", 32, 32);
}

AsteroidS::~AsteroidS() {}

eEntityType AsteroidS::getType() const
{
  return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity *getNewInstanceAsteroidS()
{
  return (new AsteroidS());
}

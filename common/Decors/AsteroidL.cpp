//
// AsteroidL.cpp for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sat May 31 17:02:48 2014 bizeul_f
// Last update Sun Jun  1 15:26:25 2014 bizeul_f
//

#include "AsteroidL.hh"

AsteroidL::AsteroidL() : AEntity(88, 83, BACKGROUND, "AsteroidL")
{
  this->_components[MOVE] = new MoveComponent((rand() % 75) * -1, rand() % 20 * ((rand() % 2) ? -1 : 1));
  this->_components[SPRITE] = new SimpleSpriteComponent("./sprites/asteroidL.png", 88, 83);
}

AsteroidL::~AsteroidL() {}

eEntityType AsteroidL::getType() const
{
  return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity *getNewInstanceAsteroidL()
{
  return (new AsteroidL());
}

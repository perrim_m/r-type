//
// AsteroidS.cpp for R-Type in /home/bizeul_f/Developpement/r-type/common/Decors
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Sat May 31 17:02:48 2014 bizeul_f
// Last update Sat May 31 18:41:46 2014 bizeul_f
//

#ifndef ASTEROIDS_HH_
# define ASTEROIDS_HH_

# include <cstdlib>

# include "AEntity.hh"
# include "SimpleSpriteComponent.hh"
# include "MoveComponent.hh"

class AsteroidS : public AEntity
{
public:
  AsteroidS();
  ~AsteroidS();

  eEntityType	getType() const;
};

#endif /* !ASTEROIDS_HH_ */

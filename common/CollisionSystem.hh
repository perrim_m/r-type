#ifndef COLLISIONSYSTEM_HH_
# define COLLISIONSYSTEM_HH_

#include <list>
#include <string>
#include "AEntity.hh"
#include "ISystem.hh"
#include "SafeQueue.hh"
#include "SerialisationServer.hh"
#include "HealthComponent.hh"
#include "BulletComponent.hh"

class			CollisionSystem : public ISystem
{
private:
  
  SerialisationServer		*_ss;
  SafeQueue<std::string>	*_toSend;

  void	quadTree(std::list<AEntity *>& touchedEntities,
		 std::list<AEntity *>& entities,
		 int deep, int x1, int y1, int sizeX, int sizeY) const;
  std::list<AEntity *>	lookForCollisions(const std::list<AEntity *>&,
					  std::list<AEntity *>) const;

public:
  CollisionSystem(SerialisationServer *, SafeQueue<std::string> *);
  virtual ~CollisionSystem();

  void	update(std::list<AEntity *> &, float);
  bool	itExistsInList(const std::list<AEntity *>&,
		       const std::list<AEntity *>::const_iterator&) const;
};

#endif /** !COLLISIONSYSTEM_HH_ **/

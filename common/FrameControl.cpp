//
//  FrameControl.cpp
//  R-Type
//
//  Created by Stern on 26/05/2014.
//
//

#include <iostream>
#include "FrameControl.hh"

FrameControl::FrameControl(float frame)
{
	this->_frame = frame;
	this->_frameRate = frame;
	this->_interval = 0;
	this->_timeLast = clock();
}

FrameControl::~FrameControl()
{
}

void	FrameControl::wait()
{
	static int fps = 0;
	static time_t timer = time(NULL);
	float			f;
	clock_t	timeNow = clock();

	f = static_cast<float>(timeNow - this->_timeLast) / CLOCKS_PER_SEC;
	if (f < this->_frame)
	{
#ifdef _WIN32
		Sleep((this->_frame - f) * 1000);
#elif __APPLE__ or __unix__
		struct timespec t;
		t.tv_sec = 0;
		t.tv_nsec = (this->_frame - f) * 1000000000;
		nanosleep(&t, NULL);
#endif
		this->_frameRate = this->_frame;
	}
	else
		this->_frameRate = f;
	this->_interval += this->_frameRate;
	fps++;
	if (timer < time(NULL))
	{
		std::cout << "FPS: " << fps << std::endl;
		fps = 0;
		timer = time(NULL);
	}
	this->_timeLast = clock();
}

bool	FrameControl::atInterval(float interval)
{
	if (this->_interval >= interval)
	{
		this->_interval -= interval;
		return (true);
	}
	return (false);
}

float	FrameControl::getFrame() const
{
	return (this->_frame);
}

float	FrameControl::getFrameRate() const
{
	return (this->_frameRate);
}

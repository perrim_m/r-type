//
//  MoveComponent.cpp
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#include "MoveComponent.hh"

MoveComponent::MoveComponent()
{
	this->_dx = 0.0;
	this->_dy = 0.0;
}

MoveComponent::MoveComponent(float dx, float dy)
{
	this->_dx = dx;
	this->_dy = dy;
}

MoveComponent::~MoveComponent()
{
}

bool	MoveComponent::update(AEntity *entity, float frameRate)
{
	float	newX;
	float	newY;

	newX = entity->getX() + (this->_dx * frameRate);
	newY = entity->getY() + (this->_dy * frameRate);
	if (entity->getType() == PLAYER)
	{
		if (newX < 0)
			newX = 0;
		if (newY < 0)
			newY = 0;
		if (newX > (800 - entity->getWidth()))
			newX = (800 - entity->getWidth());
		if (newY > (600 - entity->getHeight()))
			newY = (600 - entity->getHeight());
	}
	entity->setX(newX);
	entity->setY(newY);
	return (true);
}

float	MoveComponent::getDX() const
{
	return (this->_dx);
}

float	MoveComponent::getDY() const
{
	return (this->_dy);
}

void	MoveComponent::setDX(float dx)
{
	this->_dx = dx;
}

void	MoveComponent::setDY(float dy)
{
	this->_dy = dy;
}

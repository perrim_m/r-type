//
//  EntityManager.cpp
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#include "EntityManager.hh"

EntityManager::EntityManager()
{
#ifdef _WIN32
	HMODULE	handle;
	
	if ((handle = LoadLibrary(TEXT("./lib_windows_dymlib.dll"))) == NULL)
		throw std::runtime_error("EntityManager open dymlib for windows failed");
	if ((this->_fptr = reinterpret_cast<void *(*)(const std::string &, const std::string &)>(GetProcAddress(handle, TEXT("getNewInstanceIDymLib")))) == NULL)
		throw std::runtime_error("EntityManager getsymbol dymlib for windows failed");
#elif __APPLE__ or __unix__
	void	*handle;
# if __APPLE__
	if ((handle = dlopen("./lib_unix_dymlib.dylib", RTLD_NOW)) == NULL)
		throw std::runtime_error("EntityManager open dymlib for unix fail");
# else
	if ((handle = dlopen("./lib_unix_dymlib.so", RTLD_NOW)) == NULL)
		throw std::runtime_error("EntityManager open dymlib for unix fail");
# endif
	if ((this->_fptr = reinterpret_cast<void *(*)(const std::string &, const std::string &)>(dlsym(handle, "getNewInstanceIDymLib"))) == NULL)
		throw std::runtime_error("EntityManager getsymbol dymlib for unix fail");
#endif
}

EntityManager::~EntityManager()
{
	while (!this->_entityLib.empty())
	{
		delete this->_entityLib.begin()->second;
		this->_entityLib.erase(this->_entityLib.begin());
	}
}

void		EntityManager::loadNewLib(const std::string &entityName)
{
	std::string	path;

#ifdef _WIN32
	path = "./" + entityName + ".dll";
#elif __unix__
	path = "./lib" + entityName + ".so";
#elif __APPLE__
	path = "./lib" + entityName + ".dylib";
#endif
	if ((this->_entityLib[entityName] = reinterpret_cast<IDymLib *>(this->_fptr(path, entityName))) == NULL)
		throw std::runtime_error("LibManager get mutexlib failed");
	this->_entityLib[entityName]->openDymLib();
}

AEntity						*EntityManager::createNewEntity(const std::string &entityName)
{
	static unsigned short	id = 0;
	AEntity					*entity;

	if (this->_entityLib.find(entityName) == this->_entityLib.end())
	{
		this->loadNewLib(entityName);
	}
	entity = reinterpret_cast<AEntity *>(this->_entityLib[entityName]->symDymLib());
	entity->setId(id);
	id = (id + 1) % 100000;
	return (entity);
}
//
//  IComponent.hh
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef ICOMPONENT_HH_
# define ICOMPONENT_HH_

class AEntity;

enum	eComponent
{
	MOVE = 0,
	ANIMATION,
	SPRITE,
	WEAPON,
	INPUTKEY,
	HEALTH,
	BULLET,
	PARTICLE,
	REACTOR,
};

class IComponent
{
public:
	virtual ~IComponent() {}

	virtual bool	update(AEntity *, float) = 0;
};

#endif // !ICOMPONENT_HH_

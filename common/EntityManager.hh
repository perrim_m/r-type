//
//  EntityManager.h
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef ENTITYMANAGER_HH_
# define ENTITYMANAGER_HH_

# ifdef _WIN32
# include <Windows.h>
# elif __APPLE__ or __unix__
# include <dlfcn.h>
# endif

# include <stdexcept>
# include <limits>
# include <string>
# include <map>

# include "AEntity.hh"
# include "IDymLib.hh"

class EntityManager
{
private:
	std::map<std::string, IDymLib *>	_entityLib;

	void	*(*_fptr)(const std::string &, const std::string &);
	
	void	loadNewLib(const std::string &);

public:
	EntityManager();
	virtual ~EntityManager();

	AEntity	*createNewEntity(const std::string &);
};

#endif // !ENTITYMANAGER_HH_

//
//  BulletComponent.cpp
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#include "BulletComponent.hh"

BulletComponent::BulletComponent(int damage, eOwner owner)
{
	this->_owner = owner;
	this->_damage = damage;
}

BulletComponent::~BulletComponent()
{
}

bool	BulletComponent::update(AEntity *, float)
{
	return (true);
}

eOwner	BulletComponent::getOwner() const
{
	return (this->_owner);
}

int	BulletComponent::getDamage() const
{
	return (this->_damage);
}
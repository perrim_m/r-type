//
//  HealthComponent.cpp
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#include "HealthComponent.hh"

HealthComponent::HealthComponent(int lives, int hp)
{
	this->_lives = lives;
	this->_hp = hp;
}

HealthComponent::~HealthComponent()
{
}

bool	HealthComponent::update(AEntity *, float)
{
	return (true);
}

int	HealthComponent::getLives() const
{
	return (this->_lives);
}

int	HealthComponent::getHp() const
{
	return (this->_hp);
}

bool	HealthComponent::takeDamage(int damage)
{
	if (this->_hp > 0 && this->_hp > damage)
	{
		this->_hp -= damage;
	}
	else
	{
		if (!this->_lives)
			return (false);
		--this->_lives;
	}
	return (true);
}

//
//  LibManager.hh
//  R-Type
//
//  Created by Stern on 01/05/2014.
//
//

#ifndef LIBMANAGER_HH_
# define LIBMANAGER_HH_

# ifdef _WIN32
# include <Windows.h>
# elif __APPLE__ or __unix__
# include <dlfcn.h>
# endif

# include <stdexcept>

# include "IDymLib.hh"
# include "IMutex.hh"
# include "IThread.hh"
# include "ISocket.hh"
# include "IDisplay.hh"

class LibManager
{
private:
	IDymLib			*_mutexLib;
	IDymLib			*_threadLib;
	IDymLib			*_socketLib;
	IDymLib			*_displayLib;

public:
	LibManager();
	virtual ~LibManager();

	IMutex			*createNewMutex() const;
	IThread			*createNewThread() const;
	ISocket			*createNewSocket() const;
	IDisplay		*createNewDisplay() const;
};

#endif // !LIBMANAGER_HH_

//
//  LibManager.cpp
//  R-Type
//
//  Created by Stern on 01/05/2014.
//
//

#include "LibManager.hh"

LibManager::LibManager()
{
	void	*(*fptr)(const std::string &, const std::string &);
#ifdef _WIN32
	HMODULE	handle;
	
	SetDllDirectory("C:\\Users\\Stern\\Desktop\\r-type\\build\\Debug\\");
	if ((handle = LoadLibrary(TEXT("lib_windows_dymlib.dll"))) == NULL)
		throw std::runtime_error("LibManager open dymlib for windows failed");
	if ((fptr = reinterpret_cast<void *(*)(const std::string &, const std::string &)>(GetProcAddress(handle, TEXT("getNewInstanceIDymLib")))) == NULL)
		throw std::runtime_error("LibManager getsymbol dymlib for windows failed");
	if ((this->_mutexLib = reinterpret_cast<IDymLib *>(fptr("lib_windows_mutex.dll", "IMutex"))) == NULL)
		throw std::runtime_error("LibManager get mutexlib failed");
	if ((this->_threadLib = reinterpret_cast<IDymLib *>(fptr("lib_windows_thread.dll", "IThread"))) == NULL)
		throw std::runtime_error("LibManager get threadlib failed");
	if ((this->_socketLib = reinterpret_cast<IDymLib *>(fptr("lib_windows_socket.dll", "ISocket"))) == NULL)
		throw std::runtime_error("LibManager get socketlib failed");
	if ((this->_displayLib = reinterpret_cast<IDymLib *>(fptr("lib_display.dll", "IDisplay"))) == NULL)
		throw std::runtime_error("LibManager get displaylib failed");
#elif __unix__
	void	*handle;

	if ((handle = dlopen("./lib_unix_dymlib.so", RTLD_NOW)) == NULL)
		throw std::runtime_error("LibManager open dymlib for unix failed");
	if ((fptr = reinterpret_cast<void *(*)(const std::string &, const std::string &)>(dlsym(handle, "getNewInstanceIDymLib"))) == NULL)
		throw std::runtime_error("LibManager getsymbol dymlib for unix failed");
	if ((this->_mutexLib = reinterpret_cast<IDymLib *>(fptr("./lib_unix_mutex.so", "IMutex"))) == NULL)
		throw std::runtime_error("LibManager get mutexlib failed");
	if ((this->_threadLib = reinterpret_cast<IDymLib *>(fptr("./lib_unix_thread.so", "IThread"))) == NULL)
		throw std::runtime_error("LibManager get threadlib failed");
	if ((this->_socketLib = reinterpret_cast<IDymLib *>(fptr("./lib_unix_socket.so", "ISocket"))) == NULL)
		throw std::runtime_error("LibManager get socketlib failed");
	if ((this->_displayLib = reinterpret_cast<IDymLib *>(fptr("./lib_display.so", "IDisplay"))) == NULL)
		throw std::runtime_error("LibManager get displaylib failed");
#elif __APPLE__
	void	*handle;
	
	if ((handle = dlopen("./lib_unix_dymlib.dylib", RTLD_NOW)) == NULL)
		throw std::runtime_error("LibManager open dymlib for unix failed");
	if ((fptr = reinterpret_cast<void *(*)(const std::string &, const std::string &)>(dlsym(handle, "getNewInstanceIDymLib"))) == NULL)
		throw std::runtime_error("LibManager getsymbol dymlib for unix failed");
	if ((this->_mutexLib = reinterpret_cast<IDymLib *>(fptr("./lib_unix_mutex.dylib", "IMutex"))) == NULL)
		throw std::runtime_error("LibManager get mutexlib failed");
	if ((this->_threadLib = reinterpret_cast<IDymLib *>(fptr("./lib_unix_thread.dylib", "IThread"))) == NULL)
		throw std::runtime_error("LibManager get threadlib failed");
	if ((this->_socketLib = reinterpret_cast<IDymLib *>(fptr("./lib_unix_socket.dylib", "ISocket"))) == NULL)
		throw std::runtime_error("LibManager get socketlib failed");
	if ((this->_displayLib = reinterpret_cast<IDymLib *>(fptr("./lib_display.dylib", "IDisplay"))) == NULL)
		throw std::runtime_error("LibManager get displaylib failed");
#endif
}

LibManager::~LibManager()
{
	delete this->_mutexLib;
	delete this->_threadLib;
	delete this->_socketLib;
	delete this->_displayLib;
}

IMutex	*LibManager::createNewMutex() const
{
	if (this->_mutexLib->getStatus() == CLOSE)
		this->_mutexLib->openDymLib();
	return (reinterpret_cast<IMutex *>(this->_mutexLib->symDymLib()));
}

IThread	*LibManager::createNewThread() const
{
	if (this->_threadLib->getStatus() == CLOSE)
		this->_threadLib->openDymLib();
	return (reinterpret_cast<IThread *>(this->_threadLib->symDymLib()));
}

ISocket	*LibManager::createNewSocket() const
{
  if (this->_socketLib->getStatus() == CLOSE)
    this->_socketLib->openDymLib();
  return (reinterpret_cast<ISocket *>(this->_socketLib->symDymLib()));
}

IDisplay	*LibManager::createNewDisplay() const
{
	if (this->_displayLib->getStatus() == CLOSE)
		this->_displayLib->openDymLib();
	return (reinterpret_cast<IDisplay *>(this->_displayLib->symDymLib()));
}

//
//  InputComponent.h
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef INPUTCOMPONENT_HH_
# define INPUTCOMPONENT_HH_

# include <stdexcept>

# include "IComponent.hh"
# include "AEntity.hh"
# include "MoveComponent.hh"
# include "WeaponComponent.hh"

class InputComponent : public IComponent
{
private:
	float	_speed;

public:
	InputComponent();
	virtual ~InputComponent();

	bool	update(AEntity *, float);
	void	moveStop(AEntity *);
	void	moveUp(AEntity *);
	void	moveDown(AEntity *);
	void	moveRight(AEntity *);
	void	moveLeft(AEntity *);
	void	shoot(AEntity *);
};

#endif // !INPUTCOMPONENT_HH_

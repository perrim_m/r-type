//
//  SafeQueue.hh
//  Plazza
//
//  Created by Stern on 26/04/2014.
//
//

#ifndef SAFEQUEUE_HH_
# define SAFEQUEUE_HH_

# include <ctype.h>

# include <queue>
# include <string>

# include "IMutex.hh"

template <class T>
class SafeQueue
{
private:
	std::queue<T>	_queue;
	IMutex			*_mutex;

public:
	SafeQueue(IMutex *);
	~SafeQueue();

	void		push(T);
	T			get();
	bool		isEmpty() const;
	size_t		size() const;
	void		clear();
	void		lock();
	void		unlock();
};

#endif /* !SAFEQUEUE_HH_ */

//
// Boss1.cpp for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Monsters
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 23:14:19 2014 bizeul_f
// Last update Mon May 12 19:39:45 2014 bizeul_f
//

#include "Boss1.hh"

Boss1::Boss1() : AEntity(96.15f, 128.0f, MONSTER, "Boss1")
{
  this->_components[MOVE] = new MoveComponent(-10, 0);
  this->_components[SPRITE] = new Boss1SpriteComponent();
  this->_components[HEALTH] = new HealthComponent(1, 100);
}

Boss1::~Boss1() {}

eEntityType	Boss1::getType() const
{
  return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity      *getNewInstanceBoss1()
{
  return (new Boss1());
}

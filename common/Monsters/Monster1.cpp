//
// Monster1.cpp for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Monsters
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 23:13:40 2014 bizeul_f
// Last update Mon May 12 19:39:26 2014 bizeul_f
//

#include "Monster1.hh"

Monster1::Monster1() : AEntity(43, 40, MONSTER, "Monster1")
{
  this->_components[MOVE] = new MoveComponent(-250, 0);
  this->_components[SPRITE] = new Monster1SpriteComponent();
  this->_components[HEALTH] = new HealthComponent(1, 2);
}

Monster1::~Monster1() {}

eEntityType	Monster1::getType() const
{
  return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity      *getNewInstanceMonster1()
{
  return (new Monster1());
}

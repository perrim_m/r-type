//
// Boss1.hh for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Monsters
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 23:14:40 2014 bizeul_f
// Last update Wed May  7 23:14:41 2014 bizeul_f
//

#ifndef BOSS1_HH_
# define BOSS1_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "Boss1SpriteComponent.hh"
# include "HealthComponent.hh"

class Boss1 : public AEntity
{
public:
	Boss1();
	virtual ~Boss1();

	eEntityType	getType() const;
};

#endif // !BOSS1_HH_

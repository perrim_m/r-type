//
// Monster2.hh for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Monsters
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 23:12:59 2014 bizeul_f
// Last update Wed May  7 23:13:06 2014 bizeul_f
//

#ifndef MONSTER2_HH_
# define MONSTER2_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "Monster2SpriteComponent.hh"
# include "Monster2WeaponComponent.hh"

class Monster2 : public AEntity
{
public:
	Monster2();
	virtual ~Monster2();

	eEntityType	getType() const;
};

#endif // !MONSTER2_HH_

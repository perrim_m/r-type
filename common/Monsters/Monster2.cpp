//
// Monster2.cpp for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Monsters
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 23:13:20 2014 bizeul_f
// Last update Mon May 12 19:39:36 2014 bizeul_f
//

#include "Monster2.hh"

Monster2::Monster2() : AEntity(33, 34, MONSTER, "Monster2")
{
	this->_components[MOVE] = new MoveComponent(-100, 0);
	this->_components[SPRITE] = new Monster2SpriteComponent();
	this->_components[HEALTH] = new HealthComponent(1, 5);
	this->_components[WEAPON] = new Monster2WeaponComponent();
}

Monster2::~Monster2() {}

eEntityType	Monster2::getType() const
{
	return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity      *getNewInstanceMonster2()
{
	return (new Monster2());
}

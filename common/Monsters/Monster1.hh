//
// Monster1.hh for R-Type in /home/bizeul_f/Developpement/r-type/ECS/Monsters
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Wed May  7 23:14:02 2014 bizeul_f
// Last update Wed May  7 23:14:05 2014 bizeul_f
//

#ifndef MONSTER1_HH_
# define MONSTER1_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "Monster1SpriteComponent.hh"

class Monster1 : public AEntity
{
public:
	Monster1();
	virtual ~Monster1();

	eEntityType	getType() const;
};

#endif // !MONSTER1_HH_

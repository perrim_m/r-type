//
//  ISystem.hh
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef ISYSTEM_HH_
# define ISYSTEM_HH_

# include <list>

# include "AEntity.hh"

class ISystem
{
public:
	virtual ~ISystem() {}

	virtual void	update(std::list<AEntity *> &, float) = 0;
};

#endif // !ISYSTEM_HH_

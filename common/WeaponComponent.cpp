//
//  StandardWeaponComponent.cpp
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#include "WeaponComponent.hh"

WeaponComponent::WeaponComponent(const std::string &projectileName) : _projectileName(projectileName)
{
	this->_shoot = false;
}

WeaponComponent::~WeaponComponent()
{
}

const std::string	&WeaponComponent::getProjectileName() const
{
	return (this->_projectileName);
}

void	WeaponComponent::shoot()
{
	this->_shoot = true;
}
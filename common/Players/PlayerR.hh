//
//  Player.h
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef PLAYERR_HH_
# define PLAYERR_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "InputComponent.hh"
# include "WeaponComponent.hh"
# include "PlayerSpriteComponent.hh"
# include "PlayerWeaponComponent.hh"
# include "HealthComponent.hh"
# include "ReactorComponent.hh"
# include "ReactorSpriteComponent.hh"

class PlayerR : public AEntity
{
public:
	PlayerR();
	virtual ~PlayerR();

	eEntityType	getType() const;
};

#endif // !PLAYERR_HH_

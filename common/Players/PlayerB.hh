//
//  PlayerB.h
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#ifndef PLAYERB_HH_
# define PLAYERB_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "InputComponent.hh"
# include "WeaponComponent.hh"
# include "PlayerSpriteComponent.hh"
# include "PlayerWeaponComponent.hh"
# include "HealthComponent.hh"
# include "ReactorComponent.hh"
# include "ReactorSpriteComponent.hh"

class PlayerB : public AEntity
{
public:
	PlayerB();
	virtual ~PlayerB();
	
	eEntityType	getType() const;
};

#endif // !PLAYERB_HH_

//
//  PlayerY.cpp
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#include "PlayerY.hh"

PlayerY::PlayerY() : AEntity(49.5, 24, PLAYER, "PlayerY")
{
	this->_components[MOVE] = new MoveComponent();
	this->_components[INPUTKEY] = new InputComponent();
	this->_components[SPRITE] = new PlayerSpriteComponent("./sprites/player_yellow.png");
	this->_components[REACTOR] = new ReactorComponent(new ReactorSpriteComponent());
	this->_components[WEAPON] = new PlayerWeaponComponent();
	this->_components[HEALTH] = new HealthComponent(0, 0);
}

PlayerY::~PlayerY() {}

eEntityType	PlayerY::getType() const
{
	return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity	*getNewInstancePlayerY()
{
	return (new PlayerY());
}

//
//  Player.cpp
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#include "PlayerR.hh"

PlayerR::PlayerR() : AEntity(49.5, 24, PLAYER, "PlayerR")
{
	this->_components[MOVE] = new MoveComponent();
	this->_components[INPUTKEY] = new InputComponent();
	this->_components[WEAPON] = new PlayerWeaponComponent();
	this->_components[HEALTH] = new HealthComponent(0, 0);
	this->_components[SPRITE] = new PlayerSpriteComponent("./sprites/player_red.png");
	this->_components[REACTOR] = new ReactorComponent(new ReactorSpriteComponent());
}

PlayerR::~PlayerR() {}

eEntityType	PlayerR::getType() const
{
	return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity	*getNewInstancePlayerR()
{
	return (new PlayerR());
}

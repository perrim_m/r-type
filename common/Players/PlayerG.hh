//
//  PlayerG.h
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#ifndef PLAYERG_HH_
# define PLAYERG_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "InputComponent.hh"
# include "WeaponComponent.hh"
# include "PlayerSpriteComponent.hh"
# include "PlayerWeaponComponent.hh"
# include "HealthComponent.hh"
# include "ReactorComponent.hh"
# include "ReactorSpriteComponent.hh"

class PlayerG : public AEntity
{
public:
	PlayerG();
	virtual ~PlayerG();
	
	eEntityType	getType() const;
};

#endif // !PLAYERG_HH_

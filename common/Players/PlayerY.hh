//
//  PlayerY.h
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#ifndef PLAYERY_HH_
# define PLAYERY_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "InputComponent.hh"
# include "WeaponComponent.hh"
# include "PlayerSpriteComponent.hh"
# include "PlayerWeaponComponent.hh"
# include "HealthComponent.hh"
# include "ReactorComponent.hh"
# include "ReactorSpriteComponent.hh"

class PlayerY : public AEntity
{
public:
	PlayerY();
	virtual ~PlayerY();
	
	eEntityType	getType() const;
};

#endif // !PLAYERY_HH_

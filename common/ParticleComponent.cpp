//
//  ParticleComponent.cpp
//  R-Type
//
//  Created by Stern on 26/05/2014.
//
//

#include "ParticleComponent.hh"

ParticleComponent::ParticleComponent()
{
	this->_color = BLANK;
}

ParticleComponent::ParticleComponent(eColor color)
{
	this->_color = color;
}

ParticleComponent::~ParticleComponent()
{
}

bool	ParticleComponent::update(AEntity *, float)
{
	return (true);
}

eColor	ParticleComponent::getColor() const
{
	return (this->_color);
}
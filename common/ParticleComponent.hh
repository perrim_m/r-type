//
//  ParticleComponent.h
//  R-Type
//
//  Created by Stern on 26/05/2014.
//
//

#ifndef PARTICLECOMPONENT_HH_
# define PARTICLECOMPONENT_HH_

# include "IComponent.hh"

enum eColor
{
	WHITE,
	// etc
	BLANK,
};

class ParticleComponent : public IComponent
{
private:
	eColor	_color;

public:
	ParticleComponent(),
	ParticleComponent(eColor);
	virtual ~ParticleComponent();

	bool	update(AEntity *, float);

	eColor	getColor() const;
};

#endif // !PARTICLECOMPONENT_HH_

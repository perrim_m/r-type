//
//  MoveComponent.h
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef MOVECOMPONENT_HH_
# define MOVECOMPONENT_HH_

# include "IComponent.hh"
# include "AEntity.hh"

class MoveComponent : public IComponent
{
private:
	float	_dx;
	float	_dy;

public:
	MoveComponent();
	MoveComponent(float, float);
	virtual ~MoveComponent();

	bool	update(AEntity *, float);
	float	getDX() const;
	float	getDY() const;
	void	setDX(float);
	void	setDY(float);
};

#endif // !MOVECOMPONENT_HH_

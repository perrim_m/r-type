//
//  StarParticle.h
//  R-Type
//
//  Created by Stern on 26/05/2014.
//
//

#ifndef STARPARTICLE_HH_
# define STARPARTICLE_HH_

# include <cstdlib>

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "ParticleComponent.hh"

class StarParticle : public AEntity
{
public:
	StarParticle();
	virtual ~StarParticle();
	
	eEntityType	getType() const;
};

#endif // !STARPARTICLE_HH_

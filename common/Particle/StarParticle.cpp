//
//  StarParticle.cpp
//  R-Type
//
//  Created by Stern on 26/05/2014.
//
//

#include "StarParticle.hh"

StarParticle::StarParticle() : AEntity(0.5, 0.5, SAMPLE, "StarParticle")
{
	this->_components[MOVE] = new MoveComponent((rand() % 100) * -1, 0);
	this->_components[PARTICLE] = new ParticleComponent(WHITE);
}

StarParticle::~StarParticle()
{
}

eEntityType	StarParticle::getType() const
{
	return (this->_type);
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM AEntity	*getNewInstanceStarParticle()
{
	return (new StarParticle());
}


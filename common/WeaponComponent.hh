//
//  StandardWeaponComponent.hh
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef WEAPONCOMPONENT_HH_
# define WEAPONCOMPONENT_HH_

# include <string>

# include "IComponent.hh"
# include "AEntity.hh"

class WeaponComponent : public IComponent
{
protected:
	bool				_shoot;
	const std::string	_projectileName;

public:
	WeaponComponent(const std::string &);
	virtual ~WeaponComponent();

	virtual bool		update(AEntity *, float) = 0;
	const std::string	&getProjectileName() const;
	void				shoot();
};

#endif // !WEAPONCOMPONENT_HH_

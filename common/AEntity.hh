//
//  AEntity.hh
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef AENTITY_HH_
# define AENTITY_HH_

# include <cstddef>

# include <map>
# include <string>

# include "IComponent.hh"
# include "HealthComponent.hh"

enum	eEntityType
{
	PLAYER,
	MONSTER,
	PROJECTILE,
	BUILDING,
	BACKGROUND,
	SAMPLE,
	EXPLOSION,
};

class AEntity
{
protected:
	int									_id;
	float								_x;
	float								_y;
	float								_width;
	float								_height;
	const std::string					_name;
	const eEntityType					_type;
	std::map<eComponent, IComponent	*>	_components;

public:
	AEntity(float, float, eEntityType, const std::string &);
	AEntity(const AEntity &);
	virtual ~AEntity();

	bool				updateComponent(eComponent, float);
	IComponent			*getComponent(eComponent) const;

	int					getId() const;
	float				getX() const;
	float				getY() const;
	float				getWidth() const;
	float				getHeight() const;
	const std::string	&getName() const;
	void				setId(int);
	void				setX(float);
	void				setY(float);
	void				setWidth(float);
	void				setHeight(float);
	virtual eEntityType	getType() const = 0;
};

#endif // !ENTITY_HH_

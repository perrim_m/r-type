//
//  Decor1.hh
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef DECOR1_HH_
# define DECOR1_HH_

# include "AEntity.hh"
# include "MoveComponent.hh"
# include "SimpleSpriteComponent.hh"

class Decor1 : public AEntity
{
public:
	Decor1();
	virtual ~Decor1();

	eEntityType	getType() const;
};

#endif // !MOVE_HH_

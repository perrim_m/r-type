//
//  Decor1.cpp
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#include "Decor1.hh"

Decor1::Decor1() : AEntity(50, 50, eEntityType::BUILDING, "Decor1")
{
	this->_components[eComponent::MOVE] = new MoveComponent(0, 1);
	this->_components[eComponent::SPRITE] = new SimpleSpriteComponent("toto.png");
}

Decor1::~Decor1()
{
}

eEntityType	Decor1::getType() const
{
	return (this->_type);
}
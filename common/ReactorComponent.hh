//
//  EffectComponent.h
//  R-Type
//
//  Created by Stern on 28/05/2014.
//
//

#ifndef REACTORCOMPONENT_HH_
# define REACTORCOMPONENT_HH_

# include "SpriteComponent.hh"
# include "IComponent.hh"

class ReactorComponent : public IComponent
{
private:
	SpriteComponent	*_sprite;
	float			_posX;
	float			_posY;

public:
	ReactorComponent(SpriteComponent *);
	virtual ~ReactorComponent();

	bool	update(AEntity *, float);

	SpriteComponent	*getSprite() const;
	float			getPosX() const;
	float			getPosY() const;
};

#endif // !REACTORCOMPONENT_HH_

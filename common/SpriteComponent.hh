//
//  SpriteComponent.h
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef SPRITECOMPONENT_HH_
# define SPRITECOMPONENT_HH_

# include <string>

# include "IComponent.hh"
# include "AEntity.hh"

class SpriteComponent : public IComponent
{
protected:
	std::string	_sprite;
	float		_x;
	float		_y;
	float		_width;
	float		_height;
	float		_scale;

public:
	SpriteComponent(const std::string &, float, float, float, float, float);
	virtual ~SpriteComponent();

	virtual bool		update(AEntity *, float) = 0;
	const std::string	&getSprite() const;
	float				getX() const;
	float				getY() const;
	float				getWidth() const;
	float				getHeight() const;
	float				getScale() const;
};

#endif // !SPRITECOMPONENT_HH_

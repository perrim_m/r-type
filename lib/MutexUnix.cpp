//
//  MutexUnix.cpp
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#include "MutexUnix.hh"

MutexUnix::MutexUnix()
{
	if (pthread_mutex_init(&this->_mutex, NULL) < 0)
		throw std::runtime_error("Mutex init fail");
	this->unlock();
}

MutexUnix::~MutexUnix()
{
	if (pthread_mutex_destroy(&this->_mutex) < 0)
		throw std::runtime_error("Mutex destroy fail");
}

void	MutexUnix::lock()
{
	if (pthread_mutex_lock(&this->_mutex) < 0)
		throw std::runtime_error("Mutex lock fail");
}

void	MutexUnix::unlock()
{
	if (pthread_mutex_unlock(&this->_mutex) < 0)
		throw std::runtime_error("Mutex unlock fail");
}

bool	MutexUnix::trylock()
{
	return ((pthread_mutex_trylock(&this->_mutex) < 0) ? false : true);
}

extern "C" IMutex	*getNewInstanceIMutex()
{
	return (new MutexUnix());
}
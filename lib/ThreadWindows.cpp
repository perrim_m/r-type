#include "ThreadWindows.h"

ThreadWindows::ThreadWindows()
{
}

ThreadWindows::~ThreadWindows()
{
	this->killThread(0);
}

void	ThreadWindows::createThread(void *(*fptr)(void *), void *data)
{
	if ((this->_thread = CreateThread(NULL, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(fptr), data, 0, NULL)) == NULL)
		throw std::runtime_error("Create thread windows failed.");
}

void	*ThreadWindows::joinThread() const
{
	return (reinterpret_cast<void *>(WaitForSingleObject(this->_thread, INFINITE)));
}

void	ThreadWindows::cancelThread() const
{
	this->killThread(0);
}

void	ThreadWindows::ifCanceled() const
{
}

void	ThreadWindows::killThread(int) const
{
	CloseHandle(this->_thread);
}

extern "C" __declspec(dllexport) IThread	*getNewInstanceIThread()
{
	return (new ThreadWindows());
}
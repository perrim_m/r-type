//
//  IThread.hh
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#ifndef ITHREAD_HH_
# define ITHREAD_HH_

class IThread
{
public:
	virtual ~IThread() {}
	
	virtual void	createThread(void *(*)(void *), void *) = 0;
	virtual void	*joinThread() const = 0;
	virtual void	cancelThread() const = 0;
	virtual void	ifCanceled() const = 0;
	virtual void	killThread(int) const = 0;
};

#endif // !ITHREAD_HH_

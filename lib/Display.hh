//
// Display.hh for R-Type in /home/bizeul_f/Developpement/r-type/lib
//
// Made by Fabrice BIZEUL
// Login   <bizeul_f@epitech.eu>
//
// Started on  Thu May  1 19:27:56 2014 Fabrice BIZEUL
// Last update Sun Jun  1 21:00:46 2014 bizeul_f
//

#ifndef DISPLAY_HH_
# define DISPLAY_HH_

# include <SFML/Graphics.hpp>
# include <SFML/Audio.hpp>

# include <iostream>
# include <stdexcept>
# include <list>
# include <string>

# include "SpriteComponent.hh"
# include "ReactorComponent.hh"
# include "ParticleComponent.hh"
# include "IDisplay.hh"
# include "AEntity.hh"

class Display : public IDisplay
{
private:
  sf::RenderWindow			*_window;
  sf::Event				_event;
  std::map<std::string, sf::Texture>	_textures;
  sf::Font				_font;
  std::map<eColor, sf::Color>		_color;
  sf::SoundBuffer			_soundbuffer;
  sf::Sound				_sound;
  sf::SoundBuffer			_shootbuffer;
  sf::Sound				_shootsound;

public:
	Display();
	~Display();

	void	init(int, int, const std::string &);
	bool	ifKeyEvent();
	bool	ifKeyUp() const;
	bool	ifKeyDown() const;
	bool	ifKeyRight() const;
	bool	ifKeyLeft() const;
	bool	ifKeyShoot() const;
	bool	ifKeyEscape() const;
	bool	getMouseEvent(float *, float *);
	void	drawGame(const std::list<AEntity *> &);
	void	drawGUI(const std::list<Button *> &, const std::string &);
	void	load(const std::string &);
	void	unload();
	void	playMusic();
	void	stopMusic();
	void	playShoot();
};

#endif /* !DISPLAY_HH_ */

//
//  MutexUnix.hh
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#ifndef MUTEXUNIX_HH_
# define MUTEXUNIX_HH_

# include <pthread.h>

# include <stdexcept>

# include "IMutex.hh"

class MutexUnix : public IMutex
{
private:
	pthread_mutex_t	_mutex;

public:
	MutexUnix();
	virtual ~MutexUnix();

	void	lock();
	void	unlock();
	bool	trylock();
};

#endif // !MUTEXUNIX_HH_

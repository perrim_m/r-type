//
// Display.cpp for R-Type in /home/bizeul_f/Developpement/r-type/lib
//
// Made by Fabrice BIZEUL
// Login   <bizeul_f@epitech.eu>
//
// Started on  Thu May  1 19:27:01 2014 Fabrice BIZEUL
// Last update Sun Jun  1 22:11:21 2014 bizeul_f
//

#include "Display.hh"

Display::Display(){}

Display::~Display() {}

void	Display::init(int width, int height, const std::string &windowName)
{
	this->_window = new sf::RenderWindow(sf::VideoMode(width, height, 32), windowName);

	sf::Image icon;

	if(!icon.loadFromFile("sprites/icone.png"))
	  throw std::runtime_error("Display: can't load the icone.png");
	this->_window->setIcon(50,50,icon.getPixelsPtr());

	this->_window->setFramerateLimit(60);

	if (!this->_font.loadFromFile("./sprites/font.ttf"))
		throw std::runtime_error("Display: Can't load font.ttf.");
	this->_color[WHITE] = sf::Color::White;
	this->_color[BLANK] = sf::Color::Transparent;
	if (!_soundbuffer.loadFromFile("./sprites/music.ogg"))
		throw std::runtime_error("Display: impossible to load music.ogg");
	_sound.setBuffer(_soundbuffer);
	_sound.setLoop(true);
	if (!_shootbuffer.loadFromFile("sprites/tire.ogg"))
		throw std::runtime_error("Display: Error impossible to load tire.ogg");
	_shootsound.setBuffer(_shootbuffer);
}

bool	Display::ifKeyEvent()
{
  return (this->_window->pollEvent(this->_event));
}

bool	Display::ifKeyUp() const
{
  return (sf::Keyboard::isKeyPressed(sf::Keyboard::Up));
}

bool	Display::ifKeyDown() const
{
  return (sf::Keyboard::isKeyPressed(sf::Keyboard::Down));
}

bool	Display::ifKeyRight() const
{
  return (sf::Keyboard::isKeyPressed(sf::Keyboard::Right));
}

bool	Display::ifKeyLeft() const
{
  return (sf::Keyboard::isKeyPressed(sf::Keyboard::Left));
}

bool	Display::ifKeyShoot() const
{
	if (this->_event.type == sf::Event::KeyReleased)
		if (this->_event.key.code == sf::Keyboard::Space)
			return (true);
  return (false);
}

bool	Display::ifKeyEscape() const
{
  return ((sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || this->_event.type == sf::Event::Closed));
}

void			Display::drawGame(const std::list<AEntity *> &entities)
{
  std::string texture;

  this->_window->clear();
  for(std::list<AEntity *>::const_iterator it = entities.begin(); it != entities.end(); ++it)
    {
      if ((*it)->getType() == SAMPLE)
	{
	  sf::CircleShape circle;
	  circle.setRadius((*it)->getHeight());
	  circle.setFillColor(this->_color[reinterpret_cast<ParticleComponent *>((*it)->getComponent(PARTICLE))->getColor()]);
	  circle.setPosition((*it)->getX(), (*it)->getY());
	  this->_window->draw(circle);
	}
      else
	{
	  SpriteComponent	*sc;
	  ReactorComponent	*rc;

	  // sf::RectangleShape rec;
	  // rec.setFillColor(sf::Color::Red);
	  // rec.setPosition((*it)->getX(), (*it)->getY());
	  // rec.setSize(sf::Vector2f((*it)->getWidth(), (*it)->getHeight()));
	  // this->_window->draw(rec);

	  if ((rc = reinterpret_cast<ReactorComponent *>((*it)->getComponent(REACTOR))) != NULL)
	    {
	      sf::Sprite sprite;
	      sc = rc->getSprite();
	      texture = sc->getSprite();
	      this->load(texture);
	      sprite.setTexture(this->_textures[texture]);
	      sprite.setPosition(rc->getPosX(), rc->getPosY());
	      sprite.setTextureRect(sf::IntRect(sc->getX(), sc->getY(), sc->getWidth(), sc->getHeight()));
	      sprite.setScale(sc->getScale(), sc->getScale());
	      this->_window->draw(sprite);
	    }

	  if ((sc = reinterpret_cast<SpriteComponent *>((*it)->getComponent(SPRITE))) != NULL)
	    {
	      sf::Sprite sprite;
	      texture = sc->getSprite();
	      this->load(texture);
	      sprite.setTexture(this->_textures[texture]);
	      sprite.setPosition((*it)->getX(), (*it)->getY());
	      sprite.setTextureRect(sf::IntRect(sc->getX(), sc->getY(), sc->getWidth(), sc->getHeight()));
	      sprite.setScale(sc->getScale(), sc->getScale());
	      this->_window->draw(sprite);
	    }
	}
    }
  this->_window->display();
}

void			Display::drawGUI(const std::list<Button *> &buttons, const std::string &background)
{
	std::string texture;
	sf::Sprite	bg;

	this->_window->clear();
	this->load(background);
	bg.setTexture(this->_textures[background]);
	this->_window->draw(bg);
	for (std::list<Button *>::const_iterator it = buttons.begin(); it != buttons.end(); it++)
	{
		if (!(*it)->getTexture().empty())
		{
			sf::Sprite sprite;
			texture = (*it)->getTexture();
			this->load(texture);
			sprite.setTexture(this->_textures[texture]);
			sprite.setPosition((*it)->getX(), (*it)->getY());
			this->_window->draw(sprite);
		}
		if (!(*it)->getText().empty())
		{
			/*sf::RectangleShape rec;
			rec.setFillColor(sf::Color::Red);
			rec.setPosition((*it)->getX(), (*it)->getY());
			rec.setSize(sf::Vector2f((*it)->getWidth(), (*it)->getHeight()));
			this->_window->draw(rec);*/

			sf::Text text;
			text.setFont(this->_font);
			text.setString((*it)->getText());
			text.setCharacterSize((*it)->getSize());
			text.setStyle(sf::Text::Regular);
			text.setColor(sf::Color::White);
			text.setPosition((*it)->getX(), (*it)->getY());
			this->_window->draw(text);
		}
	}
	this->_window->display();
}

void Display::load(const std::string &texture)
{
	if (this->_textures.find(texture) != this->_textures.end())
    {
		this->_textures[texture];
		this->_textures[texture].loadFromFile(texture);
    }
}

bool Display::getMouseEvent(float *x, float *y)
{
	if(_event.type == sf::Event::MouseButtonPressed)
    {
		if (_event.mouseButton.button == sf::Mouse::Left)
	    {
			*x = _event.mouseButton.x;
			*y = _event.mouseButton.y;
			return (true);
	    }
    }
	return (false);
}

void Display::playMusic()
{
	_sound.play();
}

void Display::stopMusic()
{
	_sound.stop();
}

void Display::playShoot()
{
  _shootsound.play();
}

void Display::unload()
{
  this->_textures.clear();
}

#ifdef _WIN32
# define SYM extern "C" __declspec(dllexport)
#else
# define SYM extern "C"
#endif

SYM IDisplay	*getNewInstanceIDisplay()
{
	return (new Display());
}

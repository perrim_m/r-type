#include "SocketWindows.h"

SocketWindows::SocketWindows() : _maxSize(2048)
{
	this->_port = 0;
	this->_status = DISABLED;
}

SocketWindows::~SocketWindows()
{
	this->closeSocket();
}

void	SocketWindows::init(eProtocol protocol)
{
	WSADATA WSAData;

	WSAStartup(MAKEWORD(2, 0), &WSAData);
	if ((this->_socket = socket(AF_INET, (protocol == TCP) ? SOCK_STREAM : SOCK_DGRAM, (protocol == TCP) ? IPPROTO_TCP : IPPROTO_UDP)) == INVALID_SOCKET)
		throw std::runtime_error("Socket windows init failed.");
	this->_status = INIT;
	this->_protocol = protocol;
}

void	SocketWindows::bindSocket(const std::string &host, short port)
{
	this->_port = port;
	this->_host = host;

	SOCKADDR_IN sin;

	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = (host.empty()) ? htonl(INADDR_ANY) : inet_addr(host.c_str());

	if (bind(this->_socket, reinterpret_cast<SOCKADDR *>(&sin), sizeof(sin)) != 0)
		throw std::runtime_error("Socket windows bind failed.");
	this->_status = BIND;
}

void	SocketWindows::listenSocket(int nbclt) const
{
	if (listen(this->_socket, nbclt) != 0)
		throw std::runtime_error("Socket windows listen failed.");
}

bool	SocketWindows::connectToHost(const std::string &host, short port)
{
	if (this->_protocol == TCP)
	{
		this->_host = host;
		this->_port = port;

		SOCKADDR_IN sin;

		sin.sin_family = AF_INET;
		sin.sin_port = htons(port);
		sin.sin_addr.s_addr = inet_addr(host.c_str());

		if (connect(this->_socket, reinterpret_cast<SOCKADDR *>(&sin), sizeof(sin)) != 0)
			throw std::runtime_error("Socket windows connect failed.");
		this->_status = CONNECTED;
		return (true);
	}
	return (false);
}

ISocket					*SocketWindows::acceptNewConnection() const
{
	SocketWindows		*newSocket = NULL;
	SOCKADDR_IN			sin;
	SOCKET				socket;
	int					size = sizeof(sin);

	if ((socket = accept(this->_socket, NULL, NULL)) == INVALID_SOCKET)
		throw std::runtime_error("Socket windows accept fail");

	newSocket = new SocketWindows();
	newSocket->_socket = socket;
	newSocket->_protocol = this->_protocol;
	ZeroMemory(&sin, size);
	getpeername(socket, reinterpret_cast<SOCKADDR *>(&sin), &size);
	newSocket->_host = inet_ntoa(sin.sin_addr);
	return (newSocket);
}

void	SocketWindows::closeSocket()
{
	if (this->_status != DISABLED)
	{
		if (closesocket(this->_socket) != 0)
			throw std::runtime_error("Socket windows close failed.");
		WSACleanup();
		this->_status = DISABLED;
	}
}

long		SocketWindows::receiveData(std::string &data) const
{
	long	rd;
	char	*buffer = new char[this->_maxSize];

	data.clear();
	if ((rd = recv(this->_socket, buffer, this->_maxSize, 0)) == SOCKET_ERROR)
		throw std::runtime_error("Socket windows receive failed.");
	buffer[rd] = '\0';
	data = buffer;
	delete buffer;
	return (rd);
}

long			SocketWindows::receiveDataFrom(std::string &data, const std::string &host, short port) const
{
	long		rf;
	char		*buffer = new char[this->_maxSize];
	SOCKADDR_IN	sin;
	int			len = sizeof(sin);

	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = (host.empty()) ? htonl(INADDR_ANY) : inet_addr(host.c_str());

	data.clear();
	if ((rf = recvfrom(this->_socket, buffer, this->_maxSize, 0, reinterpret_cast<SOCKADDR *>(&sin), &len)) == SOCKET_ERROR)
		throw std::runtime_error("Socket windows receiveFrom failed.");
	buffer[rf] = '\0';
	data = buffer;
	delete buffer;
	return (rf);
}

long		SocketWindows::sendData(const std::string &data) const
{
	long	wr;
	
	if ((wr = send(this->_socket, data.c_str(), data.size(), 0)) == SOCKET_ERROR)
		throw std::runtime_error("Socket windows send failed.");
	return (wr);
}

long			SocketWindows::sendDataTo(const std::string &data, const std::string &host, short port) const
{
	long		st;
	SOCKADDR_IN	sin;
	int			len = sizeof(sin);

	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = inet_addr(host.c_str());

	if ((st = sendto(this->_socket, data.c_str(), data.size(), 0, reinterpret_cast<SOCKADDR *>(&sin), len)) == SOCKET_ERROR)
	    throw std::runtime_error("Socket windows sendTo failed.");
	return (st);
}

const std::string	&SocketWindows::getHost() const
{
	return (this->_host);
}

short	SocketWindows::getPort() const
{
	return (this->_port);
}

eProtocol	SocketWindows::getProtocol() const
{
	return (this->_protocol);
}

eStatus	SocketWindows::getStatus() const
{
	return (this->_status);
}

bool	SocketWindows::isOpen() const
{
	return (true);
}

extern "C" __declspec(dllexport) ISocket	*getNewInstanceISocket()
{
	return (new SocketWindows());
}
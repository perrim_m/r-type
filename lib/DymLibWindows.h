#ifndef DYMLIBWINDOWS_H_
# define DYMLIBWINDOWS_H_

# include <Windows.h>

# include <stdexcept>
# include <string>

# include "IDymLib.hh"

class DymLibWindows : public IDymLib
{
private:
	HMODULE				_handle;
	eLibStatus			_status;
	const std::string	_path;
	const std::string	_objName;


public:
	DymLibWindows(const std::string &, const std::string &);
	virtual ~DymLibWindows();

	void		openDymLib();
	void		closeDymLib();
	void		*symDymLib() const;
	eLibStatus	getStatus() const;
};

#endif // !DYMLIBWINDOWS_H_
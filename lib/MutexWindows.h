#pragma once
#ifndef MUTEXWINDOWS_H_
# define MUTEXWINDOWS_H_

# include <Windows.h>

# include <stdexcept>

# include "IMutex.hh"

class MutexWindows : public IMutex
{
private:
	CRITICAL_SECTION	_mutex;

public:
	MutexWindows();
	virtual ~MutexWindows();

	void	lock();
	void	unlock();
	bool	trylock();
};

#endif // !MUTEXWINDOWS_HH_

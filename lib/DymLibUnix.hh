//
//  DymLibUnix.h
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#ifndef DYMLIBUNIX_HH_
# define DYMLIBUNIX_HH_

# include <dlfcn.h>

# include <string>
# include <stdexcept>

# include "IDymLib.hh"

class DymLibUnix : public IDymLib
{
private:
	void				*_handle;
	const std::string	_path;
	const std::string	_objName;
	eLibStatus			_status;

public:
	DymLibUnix(const std::string &, const std::string &);
	virtual ~DymLibUnix();

	void		openDymLib();
	void		closeDymLib();
	void		*symDymLib() const;
	eLibStatus	getStatus() const;
};

#endif // !DYMLIBUNIX_HH_

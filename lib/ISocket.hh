//
//  ISocket.hh
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#ifndef ISOCKET_HH_
# define ISOCKET_HH_

# include <sys/types.h>

# include <string>

enum eProtocol
{
	TCP,
	UDP,
};

enum eStatus
{
	CONNECTED,
	BIND,
	INIT,
	DISABLED,
};

class ISocket
{
public:
	virtual ~ISocket() {}

	virtual void				init(eProtocol) = 0;

	virtual void				bindSocket(const std::string &, short) = 0;
	virtual void				listenSocket(int) const = 0;
	virtual ISocket				*acceptNewConnection() const = 0;
	virtual bool				connectToHost(const std::string &, short) = 0;
	virtual void				closeSocket() = 0;

	virtual long				receiveData(std::string &) const = 0;
	virtual long				receiveDataFrom(std::string &, const std::string &, short) const = 0;

	virtual long				sendData(const std::string &) const = 0;
	virtual long				sendDataTo(const std::string &, const std::string &, short) const = 0;

	virtual const std::string	&getHost() const = 0;
	virtual short				getPort() const = 0;
	virtual eProtocol			getProtocol() const = 0;
	virtual eStatus				getStatus() const = 0;

	virtual bool				isOpen() const = 0;
};

#endif // !ISOCKET_HH_

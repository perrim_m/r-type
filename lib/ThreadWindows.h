#ifndef THREADWINDOWS_H_
# define THREADWINDOWS_H_

# include <Windows.h>

# include <stdexcept>

# include "IThread.hh"

class ThreadWindows : public IThread
{
private:
	HANDLE	_thread;

public:
	ThreadWindows();
	virtual ~ThreadWindows();

	void	createThread(void *(*)(void *), void *);
	void	*joinThread() const;
	void	cancelThread() const;
	void	ifCanceled() const;
	void	killThread(int) const;
};

#endif // !THREADWINDOWS_H_
#include "DymLibWindows.h"

DymLibWindows::DymLibWindows(const std::string &path, const std::string &objName) : _path(path), _objName(objName)
{
	this->_status = CLOSE;
}

DymLibWindows::~DymLibWindows()
{
	this->closeDymLib();
}

void	DymLibWindows::openDymLib()
{
	if (this->_status == CLOSE)
	{
		if ((this->_handle = LoadLibrary(TEXT(this->_path.c_str()))) == NULL)
			throw std::runtime_error("DymLib: " + this->_objName + " open fail");
		this->_status = OPEN;
	}
}

void	DymLibWindows::closeDymLib()
{
	if (this->_status == OPEN)
	{
		if (FreeLibrary(this->_handle) == 0)
			throw std::runtime_error("DymLib: " + this->_objName + " close fail");
		this->_status = CLOSE;
	}
}

void	*DymLibWindows::symDymLib() const
{
	void	*elem = NULL;
	void	*(*func)();
	std::string name = "getNewInstance" + this->_objName;

	if (this->_status == OPEN)
	{
		if ((func = reinterpret_cast<void *(*)()>(GetProcAddress(this->_handle, TEXT(name.c_str())))) == NULL)
			throw std::runtime_error("DymLib: " + this->_objName + " getsym fail");
		elem = func();
	}
	return (elem);
}

eLibStatus	DymLibWindows::getStatus() const
{
	return (this->_status);
}

extern "C" __declspec(dllexport) IDymLib	*getNewInstanceIDymLib(const std::string &path, const std::string &objname)
{
	return (new DymLibWindows(path, objname));
}
#pragma once

#include <Windows.h>

#include <stdexcept>

#include "IMutex.hh"

class MutexWindows : public IMutex
{
private:
	HANDLE	_mutex;

public:
	MutexWindows();
	virtual ~MutexWindows();

	void	lock();
	void	unlock();
	bool	trylock();
};


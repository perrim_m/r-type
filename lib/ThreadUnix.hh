//
//  ThreadUnix.h
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#ifndef THREADUNIX_HH_
# define THREADUNIX_HH_

# include <pthread.h>
# include <signal.h>
# include <string.h>

# include <stdexcept>

# include "IThread.hh"

class ThreadUnix : public IThread
{
private:
	pthread_t	_thread;

public:
	ThreadUnix();
	virtual ~ThreadUnix();
	
	void	createThread(void *(*)(void *), void *);
	void	*joinThread() const;
	void	cancelThread() const;
	void	ifCanceled() const;
	void	killThread(int) const;
};

#endif // !THREADUNIX_HH_
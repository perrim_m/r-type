//
//  DymLibUnix.cpp
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#include "DymLibUnix.hh"

DymLibUnix::DymLibUnix(const std::string &path, const std::string &objname) : _path(path), _objName(objname)
{
	this->_status = CLOSE;
}

DymLibUnix::~DymLibUnix()
{
	this->closeDymLib();
}

void	DymLibUnix::openDymLib()
{
	if (this->_status == CLOSE)
    {
		if ((this->_handle = dlopen(this->_path.c_str(), RTLD_LAZY)) == NULL)
			throw std::runtime_error("DymLib: " + this->_objName + " open fail");
		this->_status = OPEN;
    }
}

void	DymLibUnix::closeDymLib()
{
	if (this->_status == OPEN)
	{
		if (dlclose(this->_handle) < 0)
			throw std::runtime_error("DymLib: " + this->_objName + " close fail");
		this->_status = CLOSE;
	}
}

void		*DymLibUnix::symDymLib() const
{
	void	*elem = NULL;
	void	*(*func)();
	std::string	symbol;

	if (this->_status == OPEN)
	{
		symbol = "getNewInstance" + this->_objName;
		if ((func = reinterpret_cast<void *(*)()>(dlsym(this->_handle, symbol.c_str()))) == NULL)
			throw std::runtime_error("DymLib: " + this->_objName + " getsym fail");
		elem = func();
	}
	return (elem);
}

eLibStatus	DymLibUnix::getStatus() const
{
	return (this->_status);
}

extern "C" IDymLib	*getNewInstanceIDymLib(const std::string &path, const std::string &objname)
{
	return (new DymLibUnix(path, objname));
}

//
//  IDisplay.hh
//  R-Type
//
//  Created by Stern on 02/05/2014.
//
//

#ifndef IDISPLAY_HH_
# define IDISPLAY_HH_

# include <string>
# include <list>

# include "AEntity.hh"
# include "Button.hh"

class IDisplay
{
public:
	virtual ~IDisplay() {}

	virtual void	init(int, int, const std::string &) = 0;
	virtual bool	ifKeyEvent() = 0;
	virtual bool	ifKeyUp() const = 0;
	virtual bool	ifKeyDown() const = 0;
	virtual bool	ifKeyRight() const = 0;
	virtual bool	ifKeyLeft() const = 0;
	virtual bool	ifKeyShoot() const = 0;
	virtual bool	ifKeyEscape() const = 0;
	virtual bool	getMouseEvent(float *, float *) = 0;
	virtual void	drawGame(const std::list<AEntity *> &) = 0;
	virtual void	drawGUI(const std::list<Button *> &, const std::string &) = 0;
	virtual void	load(const std::string &) = 0;
	virtual void	unload() = 0;
	virtual void	playMusic() = 0;
	virtual void	stopMusic() = 0;
	virtual void	playShoot() = 0;
};

#endif // !IDISPLAY_HH_

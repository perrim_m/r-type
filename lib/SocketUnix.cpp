//
//  SocketUnix.cpp
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#include "SocketUnix.hh"

SocketUnix::SocketUnix() : _maxSize(2048)
{
	this->_port = 0;
	this->_status = DISABLED;
}

SocketUnix::~SocketUnix()
{
	this->closeSocket();
}

void	SocketUnix::init(eProtocol protocol)
{
	if ((this->_socket = socket(AF_INET, (protocol == TCP) ? SOCK_STREAM : SOCK_DGRAM, (protocol == TCP) ? IPPROTO_TCP : IPPROTO_UDP)) < 0)
		throw std::runtime_error("Socket init fail");
	this->_status = INIT;
	this->_protocol = protocol;
}

void	SocketUnix::bindSocket(const std::string &host, short port)
{
	this->_port = port;
	this->_host = host;
	
	struct sockaddr_in socket_in;

	bzero(&socket_in, sizeof(sockaddr_in));
	socket_in.sin_family = AF_INET;
	socket_in.sin_port = htons(port);
	socket_in.sin_addr.s_addr = (host.empty()) ? htonl(INADDR_ANY) : inet_addr(host.c_str());

	if (bind(this->_socket, reinterpret_cast<struct sockaddr *>(&socket_in), sizeof(socket_in)) < 0)
		throw std::runtime_error("Socket bind fail");
	this->_status = BIND;
}

void	SocketUnix::listenSocket(int nb_clt) const
{
	if (listen(this->_socket, nb_clt) < 0)
		throw std::runtime_error("Socket listen failed.");
}

bool	SocketUnix::connectToHost(const std::string &host, short port)
{
	if (this->_protocol == TCP)
	{
		this->_host = host;
		this->_port = port;

		struct sockaddr_in	socket_in;

		socket_in.sin_family = AF_INET;
		socket_in.sin_port = htons(port);
		socket_in.sin_addr.s_addr = inet_addr(host.c_str());

		if (connect(this->_socket, reinterpret_cast<struct sockaddr *>(&socket_in), sizeof(socket_in)) < 0)
			throw std::runtime_error("Socket connect failed.");
		this->_status = CONNECTED;
		return (true);
	}
	return (false);
}

ISocket					*SocketUnix::acceptNewConnection() const
{
	SocketUnix			*newSocket = NULL;
	struct sockaddr_in	socket_in;
	int					fd;
	socklen_t			size = sizeof(socket_in);

	if ((fd = accept(this->_socket, NULL, NULL)) < 0)
		throw std::runtime_error("Socket accept fail");
	newSocket = new SocketUnix();
	newSocket->_socket = fd;
	newSocket->_protocol = this->_protocol;
	bzero(&socket_in, size);
	getpeername(fd, reinterpret_cast<struct sockaddr *>(&socket_in), &size);
	newSocket->_host = inet_ntoa(socket_in.sin_addr);
	return (newSocket);
}

void		SocketUnix::closeSocket()
{
	if (this->_status != DISABLED)
	{
		if (close(this->_socket) < 0)
			throw std::runtime_error("Socket close fail");
		this->_status = DISABLED;
	}
}

long		SocketUnix::receiveData(std::string &data) const
{
	long	r;
	char	buffer[this->_maxSize];

	data.clear();
	if ((r = recv(this->_socket, buffer, this->_maxSize, 0)) <= 0)
	  throw std::runtime_error("Socket recv failed.");
	buffer[r] = '\0';
	data = buffer;
	return (r);
}

long					SocketUnix::receiveDataFrom(std::string &data, const std::string &host, short port) const
{
	long				rf;
	char				buffer[this->_maxSize];
	struct sockaddr_in	socket_in;
	socklen_t			len = sizeof(socket_in);

	socket_in.sin_family = AF_INET;
	socket_in.sin_port = htons(port);
	socket_in.sin_addr.s_addr = (host.empty()) ? htonl(INADDR_ANY) : inet_addr(host.c_str());

	data.clear();
	if ((rf = recvfrom(this->_socket, buffer, this->_maxSize, 0, reinterpret_cast<struct sockaddr *>(&socket_in), &len)) <= 0)
	  throw std::runtime_error("Socket recvFrom failed.");
	buffer[rf] = '\0';
	data = buffer;
	return (rf);
}

long	SocketUnix::sendData(const std::string &data) const
{
	long	wr;

	if ((wr = send(this->_socket, data.c_str(), data.size(), 0)) < 0)
		throw std::runtime_error("Socket send failed.");
	return (wr);
}

long	SocketUnix::sendDataTo(const std::string &data, const std::string &host, short port) const
{
	long				st;
	struct sockaddr_in	socket_in;
	socklen_t			len = sizeof(socket_in);

	socket_in.sin_family = AF_INET;
	socket_in.sin_port = htons(port);
	socket_in.sin_addr.s_addr = inet_addr(host.c_str());

	if ((st = sendto(this->_socket, data.c_str(), data.size(), 0, reinterpret_cast<struct sockaddr *>(&socket_in), len)) < 0)
	  {
	    std::cerr << "Socket sendDataTo failed. Retry..." << std::endl;
	    if ((st = sendto(this->_socket, data.c_str(), data.size(), 0, reinterpret_cast<struct sockaddr *>(&socket_in), len)) < 0)
	      std::cerr << "Socket sendDataTo failed." << std::endl;
	  }
	return (st);
}

const std::string	&SocketUnix::getHost() const
{
	return (this->_host);
}

short	SocketUnix::getPort() const
{
	return (this->_port);
}

eProtocol	SocketUnix::getProtocol() const
{
	return (this->_protocol);
}

eStatus	SocketUnix::getStatus() const
{
	return (this->_status);
}

bool	SocketUnix::isOpen() const
{
	return (true);
}

extern "C" ISocket	*getNewInstanceISocket()
{
	return (new SocketUnix());
}

#include "MutexWindows.h"

MutexWindows::MutexWindows()
{
	InitializeCriticalSection(&this->_mutex);
}

MutexWindows::~MutexWindows()
{
	DeleteCriticalSection(&this->_mutex);
}

void	MutexWindows::lock()
{
	EnterCriticalSection(&this->_mutex);
}

void	MutexWindows::unlock()
{
	LeaveCriticalSection(&this->_mutex);
}

bool	MutexWindows::trylock()
{
	return (TryEnterCriticalSection(&this->_mutex));
}

extern "C" __declspec(dllexport) IMutex	*getNewInstanceIMutex()
{
	return (new MutexWindows());
}
#pragma once
#ifndef SOCKETWINDOWS_H_
# define SOCKETWINDOWS_H_

# include <winsock2.h>
# include <ws2tcpip.h>
# include <Windows.h>
# pragma comment(lib, "ws2_32.lib")

# include <stdexcept>

# include "ISocket.hh"

class SocketWindows : public ISocket
{
private:
	SOCKET		_socket;
	eProtocol	_protocol;
	std::string	_host;
	short		_port;
	eStatus		_status;
	const int	_maxSize;

public:
	SocketWindows();
	virtual ~SocketWindows();

	void				init(eProtocol);
	
	void				bindSocket(const std::string &, short);
	void				listenSocket(int) const;
	ISocket				*acceptNewConnection() const;
	bool				connectToHost(const std::string &, short);
	void				closeSocket();
	
	long				receiveData(std::string &) const;
	long				receiveDataFrom(std::string &, const std::string &, short) const;
	
	long				sendData(const std::string &) const;
	long				sendDataTo(const std::string &, const std::string &, short) const;
	
	const std::string	&getHost() const;
	short				getPort() const;
	eProtocol			getProtocol() const;
	eStatus				getStatus() const;

	bool				isOpen() const;
};

#endif // !SOCKETWINDOWS_H_
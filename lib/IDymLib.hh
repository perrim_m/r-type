//
//  IDymLib.hh
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#ifndef IDYMLIB_HH_
# define IDYMLIB_HH_

enum eLibStatus
{
    OPEN,
    CLOSE
};

class IDymLib
{
public:
	virtual ~IDymLib() {}

	virtual void		openDymLib() = 0;
	virtual void		closeDymLib() = 0;
	virtual	void		*symDymLib() const = 0;
	virtual eLibStatus	getStatus() const = 0;
};

#endif // !IDYMLIB_HH_

//
//  ThreadUnix.cpp
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#include "ThreadUnix.hh"

ThreadUnix::ThreadUnix()
{}

ThreadUnix::~ThreadUnix()
{}

void	ThreadUnix::createThread(void *(*fptr)(void *), void *data)
{
	bzero(&this->_thread, sizeof(this->_thread));
	if (pthread_create(&(this->_thread), NULL, fptr, data) != 0)
		throw std::runtime_error("Thread create fail");
}

void	*ThreadUnix::joinThread() const
{
	void	*data = NULL;

	if (pthread_join(this->_thread, &data) != 0)
		throw std::runtime_error("Thread join fail");
	return (data);
}

void	ThreadUnix::cancelThread() const
{
	if (pthread_cancel(this->_thread) != 0)
		throw std::runtime_error("Thread cancel fail");
}

void	ThreadUnix::ifCanceled() const
{
	pthread_testcancel();
}

void	ThreadUnix::killThread(int sig) const
{
	pthread_kill(this->_thread, sig);
}

extern "C" IThread	*getNewInstanceIThread()
{
	return (new ThreadUnix());
}
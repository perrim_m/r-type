//
//  SocketUnix.h
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#ifndef SOCKETUNIX_HH_
# define SOCKETUNIX_HH_

# include <sys/socket.h>
# include <arpa/inet.h>
# include <unistd.h>
# include <strings.h>

# include <stdexcept>
# include <iostream>

# include "ISocket.hh"

class SocketUnix : public ISocket
{
private:
	int			_socket;
	eProtocol	_protocol;
	std::string	_host;
	short		_port;
	eStatus		_status;
	const int	_maxSize;

public:
	SocketUnix();
	virtual ~SocketUnix();

	void				init(eProtocol);
	
	void				bindSocket(const std::string &, short);
	void				listenSocket(int) const;
	ISocket				*acceptNewConnection() const;
	bool				connectToHost(const std::string &, short);
	void				closeSocket();
	
	long				receiveData(std::string &) const;
	long				receiveDataFrom(std::string &, const std::string &, short) const;
	
	long				sendData(const std::string &) const;
	long				sendDataTo(const std::string &, const std::string &, short) const;
	
	const std::string	&getHost() const;
	short				getPort() const;
	eProtocol			getProtocol() const;
	eStatus				getStatus() const;

	bool				isOpen() const;
};

#endif // !SOCKETUNIX_HH_

//
//  AnimationSystem.h
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef ANIMATIONSYSTEM_HH_
# define ANIMATIONSYSTEM_HH_

# include <stdexcept>
# include <string>

# include "ISystem.hh"
# include "WeaponComponent.hh"
# include "EntityManager.hh"
# include "SafeQueue.hh"
# include "SerialisationServer.hh"

class AnimationSystem : public ISystem
{
private:
	EntityManager			*_entityManager;
	SafeQueue<std::string>	*_toSend;
	SerialisationServer		*_ss;

public:
	AnimationSystem(EntityManager *, SafeQueue<std::string> *, SerialisationServer *);
	virtual ~AnimationSystem();

	void	update(std::list<AEntity *> &, float);
};

#endif // !ANIMATIONSYSTEM_HH_

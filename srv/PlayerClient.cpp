
#include "PlayerClient.hh"
#include "RoomManager.hh"

PlayerClient::PlayerClient(ISocket *socket, IThread *thread, RoomManager *rm)
{
	this->_socket = socket;
	this->_thread = thread;
	this->_roomManager = rm;
	this->_status = LOGIN;
	this->_id = 0;
}

PlayerClient::~PlayerClient()
{
	delete this->_socket;
	delete this->_thread;
}

void					PlayerClient::run()
{
	std::string			data;
	int					type;
	int					size;
	std::string			room;
	std::string			map;
	int					id;
	std::stringstream	ss;
	unsigned long		f;
	bool				inRoom = false;
	int					idRoom = 0;

	while (true)
	{
		this->_socket->receiveData(data);
		if (this->checkPacket(data))
		{
			ss.clear();
			ss.str("");
			ss << data;
			ss >> type;
			data = data.substr(2);
			ss.clear();
			ss.str("");
			ss << data;
			ss >> size;
			if (type == 0)
			{
				if (inRoom && (*this->_roomManager)[idRoom])
				{
					(*this->_roomManager)[idRoom]->removePlayer(this->_id);
					break ;
				}
			}
			else if (type == 1)
				this->_socket->sendData(this->_ss.sendRooms(this->_roomManager->getRooms()));
			else if (type == 2)
			{
				data = data.substr(5);
				if ((f = data.find_first_of(':')) != std::string::npos)
				{
					room = data.substr(0, f);
					map = data.substr(f + 1, (size - f - 2));
					id = this->_roomManager->createRoom(room, map);
					ss.clear();
					ss.str("");
					ss << "2:" << std::setfill('0') << std::setw(2) << id << ";";
					this->_socket->sendData(ss.str());
				}
			}
			else if (type == 3)
			{
				if ((*this->_roomManager)[size])
				{
					idRoom = size;
					if ((this->_id = (*this->_roomManager)[size]->addPlayer(this)) == -1)
						break ;
					inRoom = true;
				}
			}
		}
	}
	this->_status = LOGOUT;
}

bool	PlayerClient::checkPacket(const std::string &data) const
{
	int	i = 0;

	if (!isdigit(data[i++]))
		return (false);
	if (data[i++] != ':')
		return (false);
	while (i < 6)
		if (!isdigit(data[i++]))
			return (false);
	if (data[i] != ':' && data[i] != ';')
		return (false);
	return (true);
}

IThread	*PlayerClient::getThread() const
{
	return (this->_thread);
}

ISocket	*PlayerClient::getSocket() const
{
	return (this->_socket);
}

int		PlayerClient::getId() const
{
	return (this->_id);
}

ePLayerStatus	PlayerClient::getStatus() const
{
	return (this->_status);
}
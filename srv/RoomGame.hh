

#ifndef ROOMGAME_HH_
# define ROOMGAME_HH_

# include <string>
# include <list>
# include <vector>
# include <map>

# include "IThread.hh"
# include "ISocket.hh"
# include "IMutex.hh"
# include "ISystem.hh"
# include "LibManager.hh"
# include "EntityManager.hh"
# include "AEntity.hh"
# include "SafeQueue.hh"
# include "PlayerClient.hh"
# include "AnimationSystem.hh"
# include "CollisionSystem.hh"
# include "MapSystem.hh"
# include "InputComponent.hh"
# include "SerialisationServer.hh"
# include "FrameControl.hh"

class PlayerClient;

enum eRoomStatus
{
    CREATED,
    STARTED,
    ENDED,
};

class RoomGame
{
	int							_id;
	std::string					_name;
	std::string					_mapName;
	LibManager					*_lm;
	EntityManager				*_em;
	SerialisationServer			_ss;
	std::list<PlayerClient *>	_players;
	std::list<AEntity *>		_entities;
	std::list<ISystem *>		_systems;
	int							_frame;
	SafeQueue<std::string>		_toSend;
	SafeQueue<std::string>		_received;
	IThread						*_thread;
	IMutex						*_mutex;
	ISocket						*_socket;
	eRoomStatus					_status;
	std::map<std::string, bool>	_ships;

	void	interpretEvent();
	void	sendToClients();

public:
	RoomGame(int, const std::string &, const std::string &, LibManager *, EntityManager *, ISocket *);
	virtual ~RoomGame();

	void				run();
	int					addPlayer(PlayerClient *);
	void				removePlayer(int);
	void				pushEvent(const std::string &);

	unsigned long		getNbPlayer() const;
	int					getId() const;
	const std::string	&getName() const;
	const std::string	&getMapName() const;
	IThread				*getThread() const;
	eRoomStatus			getStatus() const;
};

#endif // !ROOMGAME_HH_

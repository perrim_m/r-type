#include "Server.hh"

Server::Server() : _rm(&this->_lm, &this->_em, (this->_socketUDP = this->_lm.createNewSocket()))
{
	this->_socketTCP = this->_lm.createNewSocket();
	this->_socketTCP->init(TCP);
	this->_socketTCP->bindSocket("", 4242);
	this->_socketTCP->listenSocket(4);
	this->_socketUDP->init(UDP);
	this->_socketUDP->bindSocket("", 4243);
	this->_thread = this->_lm.createNewThread();
}

Server::~Server()
{
	delete this->_socketTCP;
	delete this->_socketUDP;
	delete this->_thread;
}

void	Server::run()
{
	ISocket	*client;
	
	this->_thread->createThread(initListenUDP, this);

	std::cout << "==================" << std::endl;
	std::cout << "=  Start server  =" << std::endl;
	std::cout << "==================" << std::endl;

	while (true)
	{
		this->clearPlayerClient();
		client = this->_socketTCP->acceptNewConnection();
		this->_players.push_front(new PlayerClient(client, this->_lm.createNewThread(), &this->_rm));
		this->_players.front()->getThread()->createThread(initNewClient, this->_players.front());
		std::cout << std::endl;
		std::cout << "*** New Player Client connected ***" << std::endl;
		std::cout << std::endl;
	}
}

void	Server::clearPlayerClient()
{
	for (std::list<PlayerClient *>::iterator it = this->_players.begin(); it != this->_players.end(); it++)
	{
		if ((*it)->getStatus() == LOGOUT)
		{
			std::list<PlayerClient *>::iterator it2 = it;
			++it2;
			delete (*it);
			this->_players.erase(it);
			std::cout << "*** Player Client deleted ***" << std::endl;
			if (it2 == this->_players.end())
				break ;
			it = it2;
		}
	}
}

void					Server::listenUDP()
{
	std::string			data;
	std::stringstream	ss;
	int					id;
	size_t				f;
	

	while (true)
	{
		ss.str("");
		ss.clear();
		data.clear();
		if (this->_socketUDP->receiveDataFrom(data, "", this->_socketUDP->getPort()) == 12)
		{
			if (*(--data.end()) == ';')
			{
				if ((f = data.find_first_of(':')) != std::string::npos)
				{
					ss << data.substr(0, f);
					ss >> id;
					if (this->_rm[id] != NULL)
					{
						this->_rm[id]->pushEvent(data.substr(f + 1));
					}
				}
			}
		}
	}
}

void	*initListenUDP(void *data)
{
	Server *server = reinterpret_cast<Server *>(data);
	server->listenUDP();
	return (NULL);
}

void	*initNewClient(void *data)
{
	PlayerClient	*playerClient = static_cast<PlayerClient*>(data);
	playerClient->run();
	return (NULL);
}

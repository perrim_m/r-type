//
//  RoomManager.cpp
//  R-Type
//
//  Created by Stern on 10/05/2014.
//
//

#include "RoomManager.hh"

RoomManager::RoomManager(LibManager *lm, EntityManager *em, ISocket *socket)
{
	this->_lm = lm;
	this->_em = em;
	this->_socket = socket;
}

RoomManager::~RoomManager()
{
	while (!this->_games.empty())
	{
		delete this->_games.begin()->second;
		this->_games.erase(this->_games.begin());
	}
}

int	RoomManager::createRoom(const std::string &name, const std::string &map)
{
	static int	id = 0;

	this->clearRooms();
	this->_games[id] = new RoomGame(id, name, map, this->_lm, this->_em, this->_socket);
	this->_games[id]->getThread()->createThread(initNewRoomGame, this->_games[id]);
	std::cout << "*** New Room id n° " << id << " ***" << std::endl;
	return (id++);
}

const std::map<int, RoomGame *>	&RoomManager::getRooms()
{
	this->clearRooms();
	return (this->_games);
}

RoomGame	*RoomManager::operator[](int id)
{
	return ((this->_games.find(id) != this->_games.end()) ? this->_games[id] : NULL);
}

void	RoomManager::clearRooms()
{
	for (std::map<int, RoomGame *>::iterator it = this->_games.begin(); it != this->_games.end(); it++)
	{
		if ((*it).second->getStatus() == ENDED)
		{
			std::map<int, RoomGame *>::iterator it2 = it;
			++it2;
			std::cout << "*** Room id n° " << (*it).second->getId() << " deleted ***" << std::endl;
			delete (*it).second;
			this->_games.erase(it);
			if (it2 == this->_games.end())
				break ;
			it = it2;
		}
	}
}

void	*initNewRoomGame(void *game)
{
	RoomGame	*roomGame = static_cast<RoomGame*>(game);
	roomGame->run();
	return (NULL);
}

#ifndef PLAYERCLIENT_HH_
# define PLAYERCLIENT_HH_

# include <string>

# include "ISocket.hh"
# include "IThread.hh"
# include "LibManager.hh"
# include "SerialisationServer.hh"

class RoomManager;

enum ePLayerStatus
{
    LOGIN,
    LOGOUT,
};

class PlayerClient
{
	ISocket				*_socket;
	IThread				*_thread;
	RoomManager			*_roomManager;
	SerialisationServer	_ss;
	int					_id;
	ePLayerStatus		_status;

	bool	checkPacket(const std::string &) const;

public:
	PlayerClient(ISocket *, IThread *, RoomManager *);
	virtual ~PlayerClient();

	void			run();

	IThread			*getThread() const;
	ISocket			*getSocket() const;
	int				getId() const;
	ePLayerStatus	getStatus() const;
};

#endif // !PLAYERCLIENT_HH_

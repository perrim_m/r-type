#ifndef MAP_HH_
# define MAP_HH_

# include <string>
# include <fstream>
# include <iostream>
# include <sstream>
# include <list>

# include "AEntity.hh"
# include "EntityManager.hh"
# include "ISystem.hh"
# include "SafeQueue.hh"
# include "SerialisationServer.hh"

class MapSystem : public ISystem
{
	std::string				_name;
	std::ifstream			_map;
	EntityManager			*_em;
	SafeQueue<std::string>	*_toSend;
	SerialisationServer		*_ss;
	float					_countFrame;

public:
	MapSystem(const std::string& mapName, EntityManager *, SafeQueue<std::string> *, SerialisationServer *);
	virtual ~MapSystem();

	const std::string		&getName() const;
	void					update(std::list<AEntity *> &, float);
};

#endif // !MAP_HH_

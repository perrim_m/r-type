#ifndef SERVER_HH_
# define SERVER_HH_

# include <string>
# include <sstream>
# include <list>

# include "PlayerClient.hh"
# include "EntityManager.hh"
# include "RoomManager.hh"
# include "LibManager.hh"
# include "ISocket.hh"
# include "IThread.hh"
# include "AEntity.hh"

class	Server
{
	LibManager					_lm;
	RoomManager					_rm;
	EntityManager				_em;
	ISocket						*_socketUDP;
	ISocket						*_socketTCP;
	IThread						*_thread;
	std::list<PlayerClient *>	_players;

	void	clearPlayerClient();

public:
	Server();
	virtual ~Server();

	void	run();
	void	clientSession();
	void	listenUDP();
};

void	*initNewClient(void *);
void	*initListenUDP(void *);

#endif // !SERVER_HH_

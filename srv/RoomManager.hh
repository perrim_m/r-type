//
//  RoomManager.hh
//  R-Type
//
//  Created by Stern on 10/05/2014.
//
//

#ifndef ROOMMANAGER_HH_
# define ROOMMANAGER_HH_

# include <map>
# include <string>

# include "LibManager.hh"
# include "RoomGame.hh"
# include "ISocket.hh"

class RoomManager
{
private:
	std::map<int, RoomGame *>	_games;
	LibManager					*_lm;
	EntityManager				*_em;
	ISocket						*_socket;

	void	clearRooms();

public:
	RoomManager(LibManager *, EntityManager *, ISocket *);
	virtual ~RoomManager();

	int								createRoom(const std::string &, const std::string &);
	const std::map<int, RoomGame *>	&getRooms();
	RoomGame						*operator[](int);
};

void	*initNewRoomGame(void *);

#endif // !ROOMMANAGER_HH_

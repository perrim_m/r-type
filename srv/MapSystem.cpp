
#include "MapSystem.hh"

MapSystem::MapSystem(const std::string& map, EntityManager *em, SafeQueue<std::string> *toSend, SerialisationServer *ss)
{
	this->_name = map + ".map";
	this->_em = em;
	this->_toSend = toSend;
	this->_ss = ss;
	this->_map.open(this->_name.c_str(), std::ifstream::in);
	if (this->_map.is_open() == false)
	{
		std::cerr << "Openning of the map " << this->_name << " failed" << std::endl;
    }
	this->_countFrame = 0;
}

MapSystem::~MapSystem()
{
	this->_map.close();
}

const std::string	&MapSystem::getName() const
{
	return (this->_name);
}

void	MapSystem::update(std::list<AEntity *> &entities, float framerate)
{
  int				frame = 0;
  float				y;
  char				buffer[248];
  std::string			line, strFrame, type_object, name, strY;
  size_t			pos1, pos2;
  AEntity			*obj;
  std::stringstream		ss;
  std::streampos		init_pos;

  this->_countFrame += framerate;

  while (true)
    {
      y = 0;
      if ((init_pos = this->_map.tellg()) == static_cast<std::streampos>(-1))
	break ;
      this->_map.getline(buffer, 248);
      line = buffer;
      pos1 = 0;
      if ((pos2 = line.find(':', 0)) == std::string::npos)
	break ;
      strFrame = line.substr(pos1, pos2 - pos1);
      pos1 = pos2 + 1;
      if ((pos2 = line.find(':', pos2 + 1)) == std::string::npos)
	break ;
      type_object = line.substr(pos1, pos2 - pos1);
      pos1 = pos2 + 1;
      if ((pos2 = line.find(':', pos2 + 1)) == std::string::npos)
	break ;
      name = line.substr(pos1, pos2 - pos1);
      pos1 = pos2 + 1;
      if ((pos2 = line.find(':', pos2 + 1)) != std::string::npos)
	break ;
      strY = line.substr(pos1, line.size() - pos1);

      ss.str(strFrame);
      ss >> frame;
      ss.clear();
      if (frame > static_cast<int>(this->_countFrame))
	{
	  this->_map.seekg(init_pos);
	  break ;
	}
      else if (frame < static_cast<int>(this->_countFrame))
	continue ;
      ss.str(strY);
      ss >> y;
      ss.clear();

      if (name == "End")
	{
	  // 	this->_toSend->push(this->_ss->removeEntity());
	}
      else
	{
	  obj = this->_em->createNewEntity(name);
	  obj->setY(y);
	  obj->setX(900);
	  entities.push_back(obj);
	  this->_toSend->push(this->_ss->addEntity(obj));
	}
    }
}

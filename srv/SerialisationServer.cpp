#include <iostream>
#include "SerialisationServer.hh"
#include "RoomGame.hh"

SerialisationServer::SerialisationServer()
{
}

SerialisationServer::~SerialisationServer()
{
}

const std::string		SerialisationServer::getFloatToStr(float f) const
{
	std::stringstream	value;

	if (f >= 0)
	{
		if (f < 1000)
			value << '0';
		if (f < 100)
			value << '0';
		if (f < 10)
			value << '0';
		value << std::setprecision(2) << std::fixed << f;
	}
	else
	{
		value << '-';
		if (f > -100)
			value << '0';
		if (f > -10)
			value << '0';
		value << std::setprecision(2) << std::fixed << (f * -1);
	}
	return (value.str());
}

const std::string		SerialisationServer::sendEntities(std::list<AEntity *> &entities) const
{
	std::stringstream	packet;
	MoveComponent		*mc;

	packet << "3:";

	for (std::list<AEntity *>::iterator it = entities.begin(); it != entities.end(); it++)
	{
		mc = reinterpret_cast<MoveComponent *>((*it)->getComponent(MOVE));
		packet << std::setfill('0') << std::setw(5) << (*it)->getId();
		packet << ":" << this->getFloatToStr((*it)->getX()) << ":" << this->getFloatToStr((*it)->getY());
		packet << ":" << this->getFloatToStr(mc->getDX()) << ":" << this->getFloatToStr(mc->getDY()) << ":";
		packet << (*it)->getName() << std::setfill(';') << std::setw(10 - static_cast<int>((*it)->getName().size())) << ';';
	}
	return (packet.str());
}

const std::string		SerialisationServer::addEntity(AEntity *entity) const
{
	std::stringstream	packet;
	MoveComponent		*mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE));

	packet << "0:" << std::setfill('0') << std::setw(5) << entity->getId();
	packet << ":" << this->getFloatToStr(entity->getX()) << ":" << this->getFloatToStr(entity->getY());
	packet << ":" << this->getFloatToStr(mc->getDX()) << ":" << this->getFloatToStr(mc->getDY()) << ":";
	packet << entity->getName() << std::setfill(';') << std::setw(10 - static_cast<int>(entity->getName().size())) << ';';
	return (packet.str());
}

const std::string		SerialisationServer::updateEntity(AEntity *entity) const
{
	std::stringstream	packet;
	MoveComponent		*mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE));
	
	packet << "1:" << std::setfill('0') << std::setw(5) << entity->getId();
	packet << ":" << this->getFloatToStr(entity->getX()) << ":" << this->getFloatToStr(entity->getY());
	packet << ":" << this->getFloatToStr(mc->getDX()) << ":" << this->getFloatToStr(mc->getDY()) << ":";
	packet << entity->getName() << std::setfill(';') << std::setw(10 - static_cast<int>(entity->getName().size())) << ';';
	return (packet.str());
}

const std::string		SerialisationServer::removeEntity(AEntity *entity) const
{
	std::stringstream	packet;
	
	packet << "2:" << std::setfill('0') << std::setw(5) << entity->getId();
	packet << ":0000.00:0000.00:0000.00:0000.00:";
	packet << entity->getName() << std::setfill(';') << std::setw(10) << ';';
	return (packet.str());
}

const std::string								SerialisationServer::sendRooms(const std::map<int, RoomGame*>& roomList) const
{
	std::stringstream							packet;

	packet << "1:";
	if (roomList.size() > 0)
		packet << std::setfill('0') << std::setw(4) << roomList.size() * 25 << ":";
	else
		packet << "0000;";
	for (std::map<int, RoomGame*>::const_iterator	it = roomList.begin(); it != roomList.end(); it++)
    {
		packet << std::setfill('0') << std::setw(2) << (*it).second->getId() << ':';
		packet << (*it).second->getNbPlayer() << ':';
		packet << (*it).second->getName() << std::setfill(';') << std::setw(10 - static_cast<int>((*it).second->getName().size())) << ';';
		packet << (*it).second->getMapName() << std::setfill(';') << std::setw(10 - static_cast<int>((*it).second->getMapName().size())) << ';';
    }
	return (packet.str());
}

const std::string		SerialisationServer::sendPlayerId(int id) const
{
	std::stringstream	ss;

	ss << "3:" << std::setfill('0') << std::setw(4) << id << ';';
	return (ss.str());
}

const std::string	SerialisationServer::refused() const
{
	return ("0:0000;");
}

/*
// Server to Client


// Menu
const std::string	SerialisationServer::create_game(bool reponse) const
{
  return ((reponse) ? "02:2:1;" : "2:2:0;");
}

const std::string	SerialisationServer::join_game(bool reponse) const
{
  return ((reponse) ? "03:2:1;" : "3:2:0;");
}




// Game
*/

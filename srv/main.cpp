//
// main.cpp for Server in /home/guigue_v/exos_cpp/RType/r-type
//
// Made by a
// Login   <guigue_v@epitech.net>
//
// Started on  Thu May  1 18:35:19 2014 a
// Last update Thu May  1 18:35:27 2014 a
//

#include <stdexcept>
#include <iostream>
#include "Server.hh"

int			main()
{
	Server	server;

	try
	{
		server.run();
	}
	catch (std::runtime_error &re)
	{
		std::cerr << re.what() << std::endl;
	}
	return (0);
}

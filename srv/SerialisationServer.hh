#ifndef SERIALISATIONSERVER_HH_
# define SERIALISATIONSERVER_HH_

# include <string>
# include <map>
# include <list>
# include <iomanip>
# include <sstream>

# include "AEntity.hh"
# include "MoveComponent.hh"

class RoomGame;

class	SerialisationServer
{
private:
	const std::string	getFloatToStr(float) const;

public:
	SerialisationServer();
	virtual ~SerialisationServer();

	const std::string	sendEntities(std::list<AEntity *> &) const;

	const std::string	addEntity(AEntity *) const;
	const std::string	updateEntity(AEntity *) const;
	const std::string	removeEntity(AEntity *) const;

	const std::string	sendRooms(const std::map<int, RoomGame*> &) const;
	const std::string	sendPlayerId(int id) const;

	const std::string	refused() const;
/*
	// Server to Client

	const std::string	confirm(*type ???*) const; // a faire

	// Menu
	const std::string	create_game(bool reponse) const;
	const std::string	join_game(bool reponse) const;
	
	const std::string	send_mapList(const std::list<Map*>&) const;
	const std::string	send_colors() const; // +available_colors // a faire

	// Game
	const std::string	send_scores(*list scores*) const; // a faire
	*/
};

#endif // !SERIALISATIONSERVER_HH_

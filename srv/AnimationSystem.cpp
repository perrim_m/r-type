//
//  AnimationSystem.cpp
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#include "AnimationSystem.hh"

AnimationSystem::AnimationSystem(EntityManager *entityManager, SafeQueue<std::string> *toSend, SerialisationServer *ss)
{
	this->_entityManager = entityManager;
	this->_toSend = toSend;
	this->_ss = ss;
}

AnimationSystem::~AnimationSystem()
{
}

void				AnimationSystem::update(std::list<AEntity *> &entities, float frameRate)
{
	WeaponComponent	*wc;
	AEntity			*projectile;

	for (std::list<AEntity *>::iterator it = entities.begin(); it != entities.end(); it++)
	{
		(*it)->updateComponent(ANIMATION, frameRate);
		(*it)->updateComponent(MOVE, frameRate);
		if	((*it)->updateComponent(WEAPON, frameRate))
		{
			if ((wc = reinterpret_cast<WeaponComponent *>((*it)->getComponent(WEAPON))) == NULL)
				throw std::runtime_error("");
			if ((projectile = this->_entityManager->createNewEntity(wc->getProjectileName())) == NULL)
				throw std::runtime_error("");
			if ((*it)->getType() == PLAYER)
				projectile->setX((*it)->getX() + (*it)->getWidth() / 1.5);
			else
				projectile->setX((*it)->getX() - (*it)->getWidth());
			projectile->setY((*it)->getY() + (*it)->getHeight() / 2);
			entities.push_back(projectile);
			this->_toSend->lock();
			this->_toSend->push(this->_ss->addEntity(projectile));
			this->_toSend->unlock();
		}
		if ((*it)->getX() < -(*it)->getWidth() || (*it)->getX() > 1000)
		{
			std::list<AEntity *>::iterator it2 = it;
			++it2;
			delete (*it);
			entities.erase(it);
			if (it2 == entities.end())
				break ;
			it = it2;
		}
	}
}
#include <iostream>
#include "RoomGame.hh"

RoomGame::RoomGame(int id, const std::string &name, const std::string &map, LibManager *lm, EntityManager *em, ISocket *socket) :
	_toSend(lm->createNewMutex()), _received(lm->createNewMutex())
{
	this->_id = id;
	this->_name = name;
	this->_mapName = map;
	this->_lm = lm;
	this->_em = em;
	this->_socket = socket;
	this->_thread = this->_lm->createNewThread();
	this->_mutex = this->_lm->createNewMutex();
	this->_systems.push_back(new MapSystem(map, this->_em, &this->_toSend, &this->_ss));
	this->_systems.push_back(new AnimationSystem(this->_em, &this->_toSend, &this->_ss));
	this->_systems.push_back(new CollisionSystem(&this->_ss, &this->_toSend));
	this->_status = CREATED;
	this->_ships["PlayerY"] = false;
	this->_ships["PlayerG"] = false;
	this->_ships["PlayerR"] = false;
	this->_ships["PlayerB"] = false;
}

RoomGame::~RoomGame()
{
}

void	RoomGame::run()
{
	FrameControl	fc(0.016f);

	while (this->_players.empty())
		fc.wait();
	std::cout << "Start room n°" << this->_id << std::endl;
	this->_status = STARTED;

	while (!this->_players.empty())
	{
		fc.wait();
		this->interpretEvent();
		for (std::list<ISystem *>::iterator it = this->_systems.begin(); it != this->_systems.end(); it++)
			(*it)->update(this->_entities, fc.getFrameRate());
		if (fc.atInterval(0.03))
		{
			this->_toSend.lock();
			this->_toSend.push(this->_ss.sendEntities(this->_entities));
			this->_toSend.unlock();
		}
		this->sendToClients();
	}
	std::cout << "*** Room n° " << this->_id << " end ***" << std::endl;
	this->_status = ENDED;
}

void					RoomGame::interpretEvent()
{
	InputComponent		*ic;
	std::string			packet;
	std::stringstream	ss;
	int					id;
	int					key;

	this->_received.lock();
	while (!this->_received.isEmpty())
	{
		packet = this->_received.get();
		if (packet[5] == ':')
		{
			ss.str("");
			ss.clear();
			ss << packet.substr(0, 5);
			ss >> id;
			for (std::list<AEntity *>::iterator it = this->_entities.begin(); it != this->_entities.end(); it++)
			{
				if ((*it)->getId() == id)
				{
					ic = reinterpret_cast<InputComponent *>((*it)->getComponent(INPUTKEY));
					ss.str("");
					ss.clear();
					ss << packet.substr(6);
					ss >> key;
					if (key != 0)
					{
						if (key & 1)
							ic->moveUp((*it));
						if (key & 2)
							ic->moveDown((*it));
						if (key & 4)
							ic->moveLeft((*it));
						if (key & 8)
							ic->moveRight((*it));
						if (key & 16)
							ic->shoot((*it));
					}
					else
						ic->moveStop((*it));
					this->_toSend.lock();
					this->_toSend.push(this->_ss.updateEntity((*it)));
					this->_toSend.unlock();
					break ;
				}
			}
		}
	}
	this->_received.unlock();
}

void			RoomGame::sendToClients()
{
	std::string	packet;
	
	this->_toSend.lock();
	while (!this->_toSend.isEmpty())
	{
		packet = this->_toSend.get();
		for (std::list<PlayerClient *>::iterator it = this->_players.begin(); it != this->_players.end(); it++)
		{
			this->_socket->sendDataTo(packet, (*it)->getSocket()->getHost(), 4244);
		}
	}
	this->_toSend.unlock();
}

int			RoomGame::addPlayer(PlayerClient *pc)
{
	AEntity	*entity = NULL;

	if (this->_players.size() >= 4)
		return (-1);
	for (std::map<std::string, bool>::iterator it = this->_ships.begin(); it != this->_ships.end(); it++)
	{
		if (!(*it).second)
		{
			entity = this->_em->createNewEntity((*it).first);
			(*it).second = true;
			break ;
		}
	}
	if (entity == NULL)
		return (-1);
	pc->getSocket()->sendData(this->_ss.sendPlayerId(entity->getId()));
	this->_toSend.lock();
	this->_toSend.push(this->_ss.addEntity(entity));
	this->_toSend.unlock();
	this->_entities.push_front(entity);
	this->_players.push_back(pc);
	std::cout << "*** New Player id n° " << entity->getId() << " ***" << std::endl;
	return (entity->getId());
}

void		RoomGame::removePlayer(int id)
{
	for (std::list<AEntity *>::iterator it = this->_entities.begin(); it != this->_entities.end(); it++)
	{
		if ((*it)->getId() == id)
		{
			this->_toSend.lock();
			this->_toSend.push(this->_ss.removeEntity((*it)));
			this->_toSend.unlock();
			this->_ships[(*it)->getName()] = false;
			delete *it;
			this->_entities.erase(it);
			std::cout << "*** Player id n° " << id << " deleted step 1/2 ***" << std::endl;
			break ;
		}
	}
	for (std::list<PlayerClient *>::iterator it = this->_players.begin(); it != this->_players.end(); it++)
	{
		if ((*it)->getId() == id)
		{
			this->_players.erase(it);
			std::cout << "*** Player id n° " << id << " deleted step 2/2 ***" << std::endl;
			break ;
		}
	}
}

unsigned long	RoomGame::getNbPlayer() const
{
	return (this->_players.size());
}

int	RoomGame::getId() const
{
	return (this->_id);
}

const std::string	&RoomGame::getName() const
{
	return (this->_name);
}

const std::string	&RoomGame::getMapName() const
{
	return (this->_mapName);
}

IThread	*RoomGame::getThread() const
{
	return (this->_thread);
}

eRoomStatus	RoomGame::getStatus() const
{
	return (this->_status);
}

void	RoomGame::pushEvent(const std::string &data)
{
	this->_received.push(data);
}

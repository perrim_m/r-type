//
// MenuCreateState.cpp for R-Type in /home/bizeul_f/Developpement/r-type/clt
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Fri May  9 18:13:43 2014 bizeul_f
// Last update Fri May  9 19:03:52 2014 bizeul_f
//

#include <iostream>
#include "MenuCreateState.hh"
#include "Game.hh"

MenuCreateState::MenuCreateState(IDisplay *display, ISocket *socket) : MenuState(display, socket)
{
  this->_buttons.push_back(new Button(50, 500, 110, 60, 50, "Back", "", MENU_LIST));
  this->_buttons.push_back(new Button(600, 500, 160, 60, 50, "Create", "", MENU_LIST));
}

MenuCreateState::~MenuCreateState() {  std::cout << "MenuCreateState destructor" << std::endl;}

int					MenuCreateState::update(int, void *data)
{
  float				x;
  float				y;
  Game			*game = reinterpret_cast<Game *>(data);
  std::string		str;
  int				id;
  FrameControl	fc(0.016f);
	
  while (true)
    {
      fc.wait();
      this->_display->drawGUI(this->_buttons, "./sprites/background_menu.png");
      if (this->_display->ifKeyEvent())
	{
	  if (this->_display->ifKeyEscape())
	    return (-1);
	  if (this->_display->getMouseEvent(&x, &y))
	    {
	      for (std::list<Button *>::const_iterator it = this->_buttons.begin(); it != this->_buttons.end(); it++)
		{
		  if ((*it)->ifCollision(x, y))
		    {
		      if ((*it)->getText() == "Create")
			{
			  this->_socket->sendData(this->_sc.create_game("room", "toto"));
			  this->_socket->receiveData(str);
			  if ((id = this->getId(str)) != -1)
			    {
			      game->changeState((*it)->getState());
			      return (id);
			    }
			}
		      else
			{
			  game->changeState((*it)->getState());
			  return (0);
			}
		    }
		}
	    }
	}
    }
  return (-1);
}

int						MenuCreateState::getId(const std::string &data) const
{
	int					id = -1;
	std::stringstream	ss;
	
	if (data.size() == 5)
	{
		if (data[0] == '2' && isdigit(data[2]) && isdigit(data[3]))
		{
			ss << data.substr(2);
			ss >> id;
		}
	}
	return (id);
}

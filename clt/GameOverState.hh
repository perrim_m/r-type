//
//  GameOverState.h
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef GAMEOVERSTATE_HH_
# define GAMEOVERSTATE_HH_

# include "MenuState.hh"
# include "FrameControl.hh"

class GameOverState : public MenuState
{
public:
	GameOverState(IDisplay *, ISocket *);
	virtual	~GameOverState();

	int	update(int, void *);
};

#endif // !GAMEOVERSTATE_HH_

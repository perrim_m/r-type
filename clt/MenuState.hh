//
//  MenuState.h
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef MENUSTATE_HH_
# define MENUSTATE_HH_

# include <list>

# include "IState.hh"
# include "ISocket.hh"
# include "IDisplay.hh"
# include "Button.hh"

class MenuState : public IState
{
protected:
	IDisplay			*_display;
	ISocket				*_socket;
	std::list<Button *>	_buttons;

public:
	MenuState(IDisplay *, ISocket *);
	virtual ~MenuState();

	virtual int	update(int, void *) = 0;
};

#endif // !MENUSTATE_HH_

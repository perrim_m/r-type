//
//  MenuMainState.h
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef MENUMAINSTATE_HH_
# define MENUMAINSTATE_HH_

# include <vector>

# include "MenuState.hh"
# include "Client.hh"
# include "FrameControl.hh"

class MenuMainState : public MenuState
{
public:
	MenuMainState(IDisplay *, ISocket *);
	virtual ~MenuMainState();

	int	update(int, void *);
};

#endif //!MENUMAINSTATE_HH_

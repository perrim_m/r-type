//
//  GameOverState.cpp
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#include <iostream>
#include "GameOverState.hh"

GameOverState::GameOverState(IDisplay *display, ISocket *socket) : MenuState(display, socket)
{
	
}

GameOverState::~GameOverState()
{
  std::cout << "GameOverState destructor" << std::endl;	
}

int	GameOverState::update(int, void *)
{
	return (0);
}

//
//  Config.cpp
//  R-Type
//
//  Created by Stern on 01/05/2014.
//
//

#include <iostream>
#include "Config.hh"

Config::Config(const std::string &path)
{
	this->_fs.open(path.c_str());
	//if (!this->_fs.is_open())
//		throw std::runtime_error("Config fail to open conf file");
}

Config::~Config()
{
	this->_fs.close();
  std::cout << "Config destructor" << std::endl;
}

bool			Config::getInfo(const std::string &key, std::string &value)
{
	std::string	line;
	long		f;

	this->_fs.seekg(this->_fs.beg);
	while (!this->_fs.eof() && std::getline(this->_fs, line))
	{
		if ((f = line.find(key, 0)) == 0)
		{
			line = line.substr(key.size() + 1);
			value = line;
			return (true);
		}
	}
	return (false);
}

void	Config::setInfo(const std::string &, const std::string &)
{
	
}

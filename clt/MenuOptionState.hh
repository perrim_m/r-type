//
//  MenuOptionState.hh
//  R-Type
//
//  Created by Stern on 22/05/2014.
//
//

#ifndef MENUOPTIONSTATE_HH_
# define MENUOPTIONSTATE_HH_

# include "MenuState.hh"
# include "Config.hh"
# include "FrameControl.hh"

class MenuOptionState : public MenuState
{
private:
	Config		*_config;

public:
	MenuOptionState(IDisplay *, ISocket *, Config *);
	virtual ~MenuOptionState();
	
	int		update(int, void *);
};

#endif // !MENUOPTIONSTATE_HH_

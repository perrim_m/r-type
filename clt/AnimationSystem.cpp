//
//  AnimationSystem.cpp
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#include <iostream>
#include "AnimationSystem.hh"

AnimationSystem::AnimationSystem()
{
}

AnimationSystem::~AnimationSystem()
{
  std::cout << "AnimationSystem destructor" << std::endl;
}

void				AnimationSystem::update(std::list<AEntity *> &entities, float frameRate)
{
	for (std::list<AEntity *>::iterator it = entities.begin(); it != entities.end(); it++)
	{
		(*it)->updateComponent(ANIMATION, frameRate);
		(*it)->updateComponent(SPRITE, frameRate);
		(*it)->updateComponent(MOVE, frameRate);
		(*it)->updateComponent(REACTOR, frameRate);
		if ((*it)->getType() == SAMPLE || (*it)->getType() == BACKGROUND)
		  {
		    if ((*it)->getX() < -(*it)->getWidth())
		      (*it)->setX(800);
		    if ((*it)->getY() < -(*it)->getHeight())
		      (*it)->setX(0);
		    else if ((*it)->getY() > 600)
		      (*it)->setY(-(*it)->getHeight());
		  }
		else
		{
			if ((*it)->getX() < -200 || (*it)->getX() > 1000)
			{
				std::list<AEntity *>::iterator it2 = it;
				++it2;
				delete (*it);
				entities.erase(it);
				if (it2 == entities.end())
					break ;
				it = it2;
			}
		}
	}
}

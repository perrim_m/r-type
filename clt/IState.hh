//
//  IState.hh
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef ISTATE_HH_
# define ISTATE_HH_

class IState
{
public:
	virtual ~IState() {}

	virtual int	update(int, void *) = 0;
};

#endif // !ISTATE_HH_

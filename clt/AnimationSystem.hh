//
//  AnimationSystem.h
//  R-Type
//
//  Created by Stern on 03/05/2014.
//
//

#ifndef ANIMATIONSYSTEM_HH_
# define ANIMATIONSYSTEM_HH_

# include <stdexcept>

# include "ISystem.hh"
# include "WeaponComponent.hh"
# include "EntityManager.hh"

class AnimationSystem : public ISystem
{
public:
	AnimationSystem();
	virtual ~AnimationSystem();

	void	update(std::list<AEntity *> &, float);
};

#endif // !ANIMATIONSYSTEM_HH_

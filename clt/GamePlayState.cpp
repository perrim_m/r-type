//
//  GamePlay.cpp
//  R-Type
//
//  Created by Stern on 02/05/2014.
//
//

#include "GamePlayState.hh"
#include "Game.hh"

GamePlayState::GamePlayState(LibManager *libManager, EntityManager *entityManager, IDisplay *display, ISocket *socket, Config *config) :
_toSend(libManager->createNewMutex()), _received(libManager->createNewMutex()), _animationSystem()
{
	this->_display = display;
	this->_socketTCP = socket;
	this->_socketUDP = libManager->createNewSocket();
	this->_config = config;
	this->_threadSend = libManager->createNewThread();
	this->_threadReceive = libManager->createNewThread();
	this->_entityManager = entityManager;
	this->_actions.resize(3);
	this->_actions[0] = &GamePlayState::addEntity;
	this->_actions[1] = &GamePlayState::updateEntity;
	this->_actions[2] = &GamePlayState::removeEntity;
}

GamePlayState::~GamePlayState()
{
  std::cout << "GamePlayState destructor" << std::endl;
	delete this->_threadSend;
	delete this->_threadReceive;
	delete this->_socketUDP;
	while (!this->_entities.empty())
	{
		delete this->_entities.front();
		this->_entities.pop_front();
	}
  std::cout << "GamePlayState destructor // end" << std::endl;
}

void	*sendToServerJump(void *data)
{
	GamePlayState *gps = reinterpret_cast<GamePlayState *>(data);

	gps->sendToServer();
	return (NULL);
}

void	*receiveFromServerJump(void *data)
{
	GamePlayState *gps = reinterpret_cast<GamePlayState *>(data);

	gps->receiveFromServer();
	return (NULL);
}

void					GamePlayState::initConnection()
{
	std::string			ports;
	std::stringstream	ss;

	if (!this->_config->getInfo("IP_SERVER", this->_host))
		this->_host = "127.0.0.1";
	if (!this->_config->getInfo("PORT_2", ports))
		ports = "4243";
	ss << ports;
	ss >> this->_port;
	this->_socketUDP->init(UDP);
	this->_socketUDP->bindSocket("", this->_port + 1);
}

int						GamePlayState::getPlayerId(const std::string &data) const
{
	std::stringstream	ss;
	int					id = -1;

	if (data.size() == 7)
	{
		if (data[0] == '3')
		{
			ss << data.substr(2, 4);
			ss >> id;
		}
	}
	return (id);
}

int					GamePlayState::update(int id, void *data)
{
	Game			*game = reinterpret_cast<Game *>(data);
	int				playerId;
	std::string		str;
	FrameControl	fc(0.016f);
	AEntity			*entity;

	this->_socketTCP->sendData(this->_sc.join_game(id));
	this->_socketTCP->receiveData(str);
	if ((playerId = this->getPlayerId(str)) == -1)
	{
		this->_socketTCP->sendData(this->_sc.logout());
		game->changeState(MENU_MAIN);
		return (0);
	}
	this->initConnection();
	this->_threadReceive->createThread(receiveFromServerJump, this);
	this->_threadSend->createThread(sendToServerJump, this);

	for (float i = 0; i <= 800; i++)
	{
		if (rand() % 2)
		{
			entity = this->_entityManager->createNewEntity("StarParticle");
			entity->setX(i);
			entity->setY(static_cast<float>(rand() % 600));
			entity->setId(-1);
			this->_entities.push_back(entity);
		}
	}

	entity = this->_entityManager->createNewEntity("Planet");
	entity->setX(0);
	entity->setY(390);
	entity->setId(-1);
	this->_entities.push_back(entity);

	entity = this->_entityManager->createNewEntity("PlanetEclipse");
	entity->setX(450);
	entity->setY(0);
	entity->setId(-1);
	this->_entities.push_back(entity);

	entity = this->_entityManager->createNewEntity("OmbreEclipse");
	entity->setX(450);
	entity->setY(0);
	entity->setId(-1);
	this->_entities.push_back(entity);

	entity = this->_entityManager->createNewEntity("AsteroidS");
	entity->setX(500);
	entity->setY(static_cast<float>(rand() % 600));
	entity->setId(-1);
	this->_entities.push_back(entity);

	entity = this->_entityManager->createNewEntity("AsteroidL");
	entity->setX(500);
	entity->setY(static_cast<float>(rand() % 600));
	entity->setId(-1);
	this->_entities.push_back(entity);

	this->_display->playMusic();
	while (true)
	{
		fc.wait();
		this->interpretEvent();
		this->_sc.initPacket(id, playerId);
		if (this->_display->ifKeyEvent())
			if (this->_display->ifKeyEscape())
				break ;
		if (this->_display->ifKeyUp())
			this->_sc.top();
		if (this->_display->ifKeyDown())
			this->_sc.down();
		if (this->_display->ifKeyRight())
			this->_sc.right();
		if (this->_display->ifKeyLeft())
			this->_sc.left();
		if (this->_display->ifKeyShoot())
		{
		    this->_display->playShoot();
		    this->_sc.shoot();
		}
		this->_toSend.push(this->_sc.getPacket());
		this->_animationSystem.update(this->_entities, fc.getFrameRate());
		this->_display->drawGame(this->_entities);
	}
	this->_display->stopMusic();
	this->_threadSend->cancelThread();
	this->_threadReceive->cancelThread();
	this->_toSend.clear();
	this->_received.clear();
	this->_socketTCP->sendData(this->_sc.logout());
	this->_socketTCP->closeSocket();
	this->_socketTCP->init(TCP);
	this->_socketUDP->closeSocket();
	while (!this->_entities.empty())
	{
		delete *this->_entities.begin();
		this->_entities.erase(this->_entities.begin());
	}
	game->changeState(MENU_MAIN);
	return (0);
}

void								GamePlayState::interpretEvent()
{
	std::string			packet;
	std::string			name;
	std::stringstream	ss;
	int					id;
	float				x;
	float				y;
	float				dx;
	float				dy;

	this->_received.lock();
	while (!this->_received.isEmpty())
	{
		packet = this->_received.get();
		if (packet[0] == '0' || packet[0] == '1' || packet[0] == '2')
		{
			ss.str("");
			ss.clear();
			ss << packet.substr(2, 5);
			ss >> id;
			ss.str("");
			ss.clear();
			ss << packet.substr(8, 7);
			ss >> x;
			ss.str("");
			ss.clear();
			ss << packet.substr(16, 7);
			ss >> y;
			ss.str("");
			ss.clear();
			ss << packet.substr(24, 7);
			ss >> dx;
			ss.str("");
			ss.clear();
			ss << packet.substr(32, 7);
			ss >> dy;
			name = packet.substr(40);
			name = name.substr(0, name.find_first_of(";"));
			(this->*(this->_actions[atoi(&packet[0])]))(id, x, y, dx, dy, name);
		}
		else if (packet[0] == '3')
		{
			this->updateEntities(packet.substr(2));
		}
	}
	this->_received.unlock();
}

void					GamePlayState::updateEntities(const std::string &packet)
{
	std::string			str;
	unsigned long		i = 0;
	std::string			name;
	std::stringstream	ss;
	int					id;
	float				x;
	float				y;
	float				dx;
	float				dy;

	while (packet.size() >= (i + 48))
	{
		str = packet.substr(i, 48);

		ss.str("");
		ss.clear();
		ss << str.substr(0, 5);
		ss >> id;
		ss.str("");
		ss.clear();
		ss << str.substr(6, 7);
		ss >> x;
		ss.str("");
		ss.clear();
		ss << str.substr(14, 7);
		ss >> y;
		ss.str("");
		ss.clear();
		ss << str.substr(22, 7);
		ss >> dx;
		ss.str("");
		ss.clear();
		ss << str.substr(30, 7);
		ss >> dy;
		name = str.substr(38);
		name = name.substr(0, name.find_first_of(";"));
		this->updateEntity(id, x, y, dx, dy, name);
		i += 48;
	}
}

void				GamePlayState::addEntity(int id, float x, float y, float dx, float dy, const std::string &name)
{
	AEntity			*entity;
	MoveComponent	*mc;

	if ((entity = this->_entityManager->createNewEntity(name)) != NULL)
	{
		entity->setId(id);
		entity->setX(x);
		entity->setY(y);
		mc = reinterpret_cast<MoveComponent *>(entity->getComponent(MOVE));
		mc->setDX(dx);
		mc->setDY(dy);
		this->_entities.push_back(entity);
	}
}

void								GamePlayState::updateEntity(int id, float x, float y, float dx, float dy, const std::string &name)
{
	std::list<AEntity *>::iterator	it;
	MoveComponent					*mc;

	for (it = this->_entities.begin(); it != this->_entities.end(); it++)
	{
		if ((*it)->getId() == id)
		{
			(*it)->setX(x);
			(*it)->setY(y);
			mc = reinterpret_cast<MoveComponent *>((*it)->getComponent(MOVE));
			mc->setDX(dx);
			mc->setDY(dy);
			break ;
		}
	}
	if (it == this->_entities.end())
		this->addEntity(id, x, y, dx, dy, name);
}

void	GamePlayState::removeEntity(int id, float, float, float, float, const std::string &)
{
	for (std::list<AEntity *>::iterator	it = this->_entities.begin(); it != this->_entities.end(); it++)
	{
		if ((*it)->getId() == id)
		{
			delete *it;
			this->_entities.erase(it);
			break ;
		}
	}
}

void	GamePlayState::sendToServer()
{
	while (true)
	{
		this->_threadSend->ifCanceled();
		this->_toSend.lock();
		this->_threadSend->ifCanceled();
		if (!this->_toSend.isEmpty())
		{
			this->_socketUDP->sendDataTo(this->_toSend.get(), this->_host, this->_port);
		}
		this->_threadSend->ifCanceled();
		this->_toSend.unlock();
	}
}

void					GamePlayState::receiveFromServer()
{
	std::string			data;

	while (true)
	{
		this->_threadReceive->ifCanceled();
		this->_socketUDP->receiveDataFrom(data, this->_host, this->_port);
		this->_threadReceive->ifCanceled();
		this->_received.lock();
		this->_threadReceive->ifCanceled();
		this->_received.push(data);
		this->_threadReceive->ifCanceled();
		this->_received.unlock();
	}
}

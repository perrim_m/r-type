//
//  Button.cpp
//  R-Type
//
//  Created by Stern on 01/05/2014.
//
//

#include <iostream>
#include "Button.hh"

Button::Button(float x, float y, float width, float height, unsigned int size, const std::string &text, const std::string &texture, eState state) :
	_text(text), _texture(texture)
{
	this->_x = x;
	this->_y = y;
	this->_width = width;
	this->_height = height;
	this->_size = size;
	this->_state = state;
}

Button::~Button()
{
  std::cout << "Button destructor" << std::endl;
}

float	Button::getX() const
{
	return (this->_x);
}

float	Button::getY() const
{
	return (this->_y);
}

float	Button::getWidth() const
{
	return (this->_width);
}

float	Button::getHeight() const
{
	return (this->_height);
}

unsigned int	Button::getSize() const
{
	return (this->_size);
}

const std::string	&Button::getTexture() const
{
	return (this->_texture);
}

const std::string	&Button::getText() const
{
	return (this->_text);
}

eState	Button::getState() const
{
	return (this->_state);
}

bool	Button::ifCollision(float x, float y) const
{
	return (((x >= this->_x && x <= (this->_x + this->_width)) &&
			 (y >= this->_y && y <= (this->_y + this->_height))) ? true : false);
}

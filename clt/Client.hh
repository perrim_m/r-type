//
//  Client.hh
//  R-Type
//
//  Created by Stern on 20/05/2014.
//
//

#ifndef CLIENT_HH_
# define CLIENT_HH_

enum eState
{
    MENU_MAIN,
    MENU_LIST,
    MENU_CREATE,
	MENU_OPTION,
	GAMEPLAY,
	GAMEOVER,
	REFRESH,
	QUIT,
	NONE,
};

#endif // !CLIENT_HH_

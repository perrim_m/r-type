#ifndef SERIALISATIONCLIENT_HH_
# define SERIALISATIONCLIENT_HH_

# include <string>
# include <sstream>
# include <iomanip>

class	SerialisationClient
{
private:
	std::string	_packet;
	int			_keys;

public:
	SerialisationClient();
	virtual ~SerialisationClient();

	void				initPacket(int, int);
	const std::string	getPacket();

	void				top();
	void				down();
	void				left();
	void				right();
	void				shoot();

	const std::string	get_rooms() const;
	const std::string	create_game(const std::string& room_name, const std::string& map) const;
	const std::string	join_game(int room_id) const;
	const std::string	logout() const;
	
	/*
  // Client to Server
  const std::string	login() const;
  

  // Menu
  
  const std::string	get_mapList() const;
  const std::string	get_colors(int room_id) const;
  const std::string	get_scores() const;
*/
};

#endif // !SERIALISATIONCLIENT_HH_

//
//  MenuState.cpp
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#include <iostream>
#include "MenuState.hh"

MenuState::MenuState(IDisplay *display, ISocket *socket)
{
	this->_display = display;
	this->_socket = socket;
}

MenuState::~MenuState()
{
  std::cout << "MenuState destructor" << std::endl;
	if (!this->_buttons.empty())
	{
		delete this->_buttons.front();
		this->_buttons.pop_front();
	}
  std::cout << "MenuState destructor // end" << std::endl;
}

//
//  Game.hh
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef GAME_HH_
# define GAME_HH_

# include <map>
# include <string>
# include <sstream>
# include <iostream>

# include "IState.hh"
# include "GamePlayState.hh"
# include "MenuMainState.hh"
# include "MenuListState.hh"
# include "MenuCreateState.hh"
# include "GameOverState.hh"
# include "MenuOptionState.hh"
# include "LibManager.hh"
# include "EntityManager.hh"
# include "ISocket.hh"
# include "IDisplay.hh"
# include "Config.hh"
# include "Client.hh"
# include "SerialisationClient.hh"

class IState;

class Game
{
private:
	std::map<eState ,IState *>	_states;
	IState						*_actualState;
	LibManager					_libManager;
	EntityManager				_entityManager;
	Config						_config;
	ISocket						*_socket;
	IDisplay					*_display;
	SerialisationClient			_sc;

public:
	Game();
	virtual ~Game();

	void	runGame();
	void	changeState(eState);
};

#endif // !GAME_HH_

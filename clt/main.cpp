//
//  main.cpp
//  R-Type
//
//  Created by Stern on 30/04/2014.
//
//

#include <stdexcept>
#include <iostream>
#include "Game.hh"

int			main()
{
    Game	game;

	try
	{
		game.runGame();
	}
	catch (std::runtime_error &re)
	{
		std::cerr << re.what() << std::endl;
	}
	return (0);
}

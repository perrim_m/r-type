
#include <iostream>
#include "SerialisationClient.hh"

SerialisationClient::SerialisationClient()
{
}

SerialisationClient::~SerialisationClient()
{
  std::cout << "Serialisation destructor" << std::endl;
}

void	SerialisationClient::initPacket(int gameId, int playerId)
{
	std::stringstream	ss;
	
	this->_keys = 0;
	if (gameId < 10)
		ss << '0';
	ss << gameId << ":";
	ss << std::setfill('0') << std::setw(5) << playerId;
	this->_packet = ss.str();
}

const std::string		SerialisationClient::getPacket()
{
	std::stringstream	data;

	data << this->_packet << ":";
	if (this->_keys < 10)
		data << '0';
	data << this->_keys << ";";
	this->_packet.clear();
	return (data.str());
}

void	SerialisationClient::top()
{
	this->_keys |= 1;
}

void	SerialisationClient::down()
{
	this->_keys |= 2;
}

void	SerialisationClient::left()
{
	this->_keys |= 4;
}

void	SerialisationClient::right()
{
	this->_keys |= 8;
}

void	SerialisationClient::shoot()
{
	this->_keys |= 16;
}

const std::string	SerialisationClient::get_rooms() const
{
	return ("1:0000;");
}

const std::string		SerialisationClient::create_game(const std::string& roomGame, const std::string& map) const
{
	std::stringstream	ss;

	ss << "2:" << std::setfill('0') << std::setw(4) << (roomGame.size() + map.size() + 2);
	ss << ':' << roomGame << ':' << map << ';';
	return (ss.str());
}

const std::string		SerialisationClient::join_game(int roomId) const
{
	std::stringstream	ss;
	
	ss << "3:" << std::setfill('0') << std::setw(4) << roomId << ';';
	return (ss.str());
}

const std::string	SerialisationClient::logout() const
{
	return ("0:0000;");
}

/*
** La serialisation se fait en format :
** type:size:message;
*/
/*
// Client to Server
const std::string	SerialisationClient::login() const
{
  return ("00:0;");
}



// Menu






const std::string	SerialisationClient::get_mapList() const
{
  return ("05:0;");
}

const std::string	SerialisationClient::get_colors(int roomId) const
{
  std::stringstream	ss;

  ss << roomId;
  return ("06:3:" + ss.str() + ';');
}

const std::string	SerialisationClient::get_scores() const
{
  return ("07:0;");
}
*/

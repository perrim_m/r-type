//
//  MenuCreateState.h
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef MENUCREATESTATE_HH_
# define MENUCREATESTATE_HH_

# include <string>
# include <sstream>

# include "MenuState.hh"
# include "SerialisationClient.hh"
# include "FrameControl.hh"

class MenuCreateState : public MenuState
{
private:
	SerialisationClient	_sc;

	int		getId(const std::string &) const;

public:
	MenuCreateState(IDisplay *, ISocket *);
	virtual ~MenuCreateState();
	
	int	update(int, void *);
};

#endif // !MENUCREATESTATE_HH_

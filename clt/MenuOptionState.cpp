//
//  MenuOptionState.cpp
//  R-Type
//
//  Created by Stern on 22/05/2014.
//
//

#include "MenuOptionState.hh"
#include "Game.hh"
#include <iostream>

MenuOptionState::MenuOptionState(IDisplay *display, ISocket *socket, Config *config) : MenuState(display, socket)
{
	this->_config = config;
	this->_buttons.push_back(new Button(450, 400, 70, 60, 50, "Ok", "", MENU_MAIN));
	this->_buttons.push_back(new Button(300, 400, 110, 60, 50, "Back", "", MENU_MAIN));
}

MenuOptionState::~MenuOptionState()
{
  std::cout << "MenuOptionState destructor" << std::endl;
}

int					MenuOptionState::update(int, void *data)
{
	float			x;
	float			y;
	Game			*game = reinterpret_cast<Game *>(data);
	FrameControl	fc(0.016f);

	while (true)
    {
		fc.wait();
		this->_display->drawGUI(this->_buttons, "./sprites/background_menu.png");
		if (this->_display->ifKeyEvent())
		{
			if (this->_display->ifKeyEscape())
				return (0);
			if (this->_display->getMouseEvent(&x, &y))
			{
				for (std::list<Button *>::const_iterator it = this->_buttons.begin(); it != this->_buttons.end(); it++)
				{
					if ((*it)->ifCollision(x, y))
					{
						game->changeState((*it)->getState());
						return (0);
					}
				}
			}
		}
	}
	return (-1);
}

//
//  MenuListState.h
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#ifndef MENULISTSTATE_HH_
# define MENULISTSTATE_HH_

# include <iomanip>
# include <vector>
# include <stdexcept>

# include "MenuState.hh"
# include "Config.hh"
# include "SerialisationClient.hh"
# include "FrameControl.hh"

class MenuListState : public MenuState
{
private:
	Config				*_config;
	SerialisationClient	_sc;
	std::vector<int>	_ids;

	void	getGameList();
	void	connectToServer();
	void	clearList();

public:
	MenuListState(IDisplay *, ISocket *, Config *);
	virtual ~MenuListState();

	int	update(int, void *);
};

#endif // !MENULISTSTATE_HH_

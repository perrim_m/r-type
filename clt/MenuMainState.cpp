//
// MenuMainState.cpp for R-Type in /home/bizeul_f/Developpement/r-type
//
// Made by bizeul_f
// Login   <bizeul_f@epitech.net>
//
// Started on  Fri May  9 18:11:39 2014 bizeul_f
// Last update Fri May  9 19:03:04 2014 bizeul_f
//

#include "Game.hh"
#include "MenuMainState.hh"

MenuMainState::MenuMainState(IDisplay *display, ISocket *socket) : MenuState(display, socket)
{
	this->_buttons.push_back(new Button(500, 200, 100, 60, 50, "Play", "", MENU_LIST));
	this->_buttons.push_back(new Button(500, 300, 170, 60, 50, "Option", "", MENU_OPTION));
	this->_buttons.push_back(new Button(500, 400, 110, 60, 50, "Quit", "", QUIT));
}

MenuMainState::~MenuMainState() {  std::cout << "MenuMainState destructor" << std::endl;}

int					MenuMainState::update(int, void *data)
{
	float			x;
	float			y;
	Game			*game = reinterpret_cast<Game *>(data);
	FrameControl	fc(0.016f);

	while (true)
    {
		fc.wait();
		this->_display->drawGUI(this->_buttons, "./sprites/background_menu.png");
		if (this->_display->ifKeyEvent())
		{
			if (this->_display->ifKeyEscape())
				return (-1);
			if (this->_display->getMouseEvent(&x, &y))
			{
				for (std::list<Button *>::const_iterator it = this->_buttons.begin(); it != this->_buttons.end(); it++)
				{
					if ((*it)->ifCollision(x, y))
					{
						if ((*it)->getState() == QUIT)
							return (-1);
						game->changeState((*it)->getState());
						return (0);
					}
				}
			}
		}
    }
	return (-1);
}

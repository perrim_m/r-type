//
//  Game.cpp
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#include "Game.hh"
#include <iostream>

Game::Game() : _config("./Rtype.conf")
{
	// INIT NETWORK
	this->_socket = this->_libManager.createNewSocket();
	this->_socket->init(TCP);
	// INIT DISPLAY
	this->_display = this->_libManager.createNewDisplay();
	this->_display->init(800, 600, "R-Type");
	// INIT STATES
	this->_states[MENU_MAIN] = new MenuMainState(this->_display, this->_socket);
	this->_states[MENU_LIST] = new MenuListState(this->_display, this->_socket, &this->_config);
	this->_states[MENU_CREATE] = new MenuCreateState(this->_display, this->_socket);
	this->_states[GAMEPLAY] = new GamePlayState(&this->_libManager, &this->_entityManager, this->_display, this->_socket, &this->_config);
	this->_states[GAMEOVER] = new GameOverState(this->_display, this->_socket);
	this->_states[MENU_OPTION] = new MenuOptionState(this->_display, this->_socket, &this->_config);
	this->_actualState = this->_states[MENU_MAIN];
}

Game::~Game()
{
  std::cout << "Game destructor" << std::endl;
	delete this->_socket;
	delete this->_display;
	while (!this->_states.empty())
	{
		delete this->_states.begin()->second;
		this->_states.erase(this->_states.begin());
	}
  std::cout << "Game destructor // end" << std::endl;
}

void	Game::runGame()
{
	int	rt = 0;

	while (true)
	{
		if ((rt = this->_actualState->update(rt, this)) == -1)
		{
			if (this->_socket->getStatus() == CONNECTED)
				this->_socket->sendData(this->_sc.logout());
			break ;
		}
	}
}

void	Game::changeState(eState state)
{
	if (this->_states.find(state) != this->_states.end())
	{
		this->_actualState = this->_states[state];
	}
}

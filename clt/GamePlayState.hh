//
//  GamePlay.hh
//  R-Type
//
//  Created by Stern on 02/05/2014.
//
//

#ifndef GAMEPLAYSTATE_HH_
# define GAMEPLAYSTATE_HH_

# include <cstdlib>

# include <list>
# include <vector>
# include <sstream>

# include "LibManager.hh"
# include "EntityManager.hh"
# include "SafeQueue.hh"
# include "IState.hh"
# include "ISocket.hh"
# include "IDisplay.hh"
# include "IThread.hh"
# include "IMutex.hh"
# include "AEntity.hh"
# include "AnimationSystem.hh"
# include "InputComponent.hh"
# include "SerialisationClient.hh"
# include "Config.hh"
# include "FrameControl.hh"

class GamePlayState : public IState
{
private:
	ISocket					*_socketTCP;
	ISocket					*_socketUDP;
	IDisplay				*_display;
	IThread					*_threadSend;
	IThread					*_threadReceive;
	EntityManager			*_entityManager;
	std::list<AEntity *>	_entities;
	SafeQueue<std::string>	_toSend;
	SafeQueue<std::string>	_received;
	AnimationSystem			_animationSystem;
	AEntity					*_player;
	SerialisationClient		_sc;
	Config					*_config;
	std::string				_host;
	short					_port;
	
	typedef void (GamePlayState::*fptr)(int, float, float, float, float, const std::string &);
	std::vector<fptr>		_actions;

	void	interpretEvent();
	void	updateEntities(const std::string &);
    void	addEntity(int, float, float, float, float, const std::string &);
	void	updateEntity(int, float, float, float, float, const std::string &);
	void	removeEntity(int, float, float, float, float, const std::string &);
	void	initConnection();
	int		getPlayerId(const std::string &) const;

public:
	GamePlayState(LibManager *, EntityManager *, IDisplay *, ISocket *, Config *);
	virtual ~GamePlayState();

	int		update(int, void *);
	void	sendToServer();
	void	receiveFromServer();
};

#endif // !GAMEPLAYSTATE_HH_

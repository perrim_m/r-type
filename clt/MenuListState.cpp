//
//  MenuListState.cpp
//  R-Type
//
//  Created by Stern on 04/05/2014.
//
//

#include "MenuListState.hh"
#include "Game.hh"

MenuListState::MenuListState(IDisplay *display, ISocket *socket, Config *config) : MenuState(display, socket)
{
	this->_config = config;
	this->_buttons.push_back(new Button(50, 500, 110, 60, 50, "Back", "", MENU_MAIN));
	this->_buttons.push_back(new Button(600, 120, 170, 60, 50, "Refresh", "", REFRESH));
	this->_buttons.push_back(new Button(600, 200, 160, 60, 50, "Create", "", MENU_CREATE));
}

MenuListState::~MenuListState()
{
  std::cout << "MenuListState destructor" << std::endl;
}

int					MenuListState::update(int, void *data)
{
  float				x;
  float				y;
  Game			*game = reinterpret_cast<Game *>(data);
  FrameControl	fc(0.016f);

  if (this->_socket->getStatus() == INIT)
    this->connectToServer();
  this->getGameList();
  while (true)
    {
      fc.wait();
      this->_display->drawGUI(this->_buttons, "./sprites/background_menu.png");
      if (this->_display->ifKeyEvent())
	{
	  if (this->_display->ifKeyEscape())
	    return (-1);
	  if (this->_display->getMouseEvent(&x, &y))
	    {
	      for (std::list<Button *>::const_iterator it = this->_buttons.begin(); it != this->_buttons.end(); it++)
		{
		  if ((*it)->ifCollision(x, y))
		    {
		      if ((*it)->getState() == REFRESH)
			this->getGameList();
		      else
			{
			  game->changeState((*it)->getState());
			  if ((*it)->getState() == GAMEPLAY)
			    return (this->_ids[((*it)->getY() -150) / 40]);
			  return (0);
			}
		    }
		}
	    }
	}
    }
  return (-1);
}

void	MenuListState::connectToServer()
{
	std::list<Button *>::iterator it = this->_buttons.end();
	std::string			host;
	std::string			port1s;
	std::stringstream	ss;
	short				port1;
	
	if (!this->_config->getInfo("IP_SERVER", host))
		host = "127.0.0.1";
	if (!this->_config->getInfo("PORT_1", port1s))
		port1s = "4242";
	ss << port1s;
	ss >> port1;
	--it;
	try
	{
		this->_socket->connectToHost(host, port1);
		if ((*it)->getState() == NONE)
			this->_buttons.erase(it);
	}
	catch (std::runtime_error &re)
	{
		if ((*it)->getState() != NONE)
			this->_buttons.push_back(new Button(200, 300, 0, 0, 30, "No connection to server.", "", NONE));
	}
}

void					MenuListState::getGameList()
{
	std::string			data;
	int					type;
	int					size;
	int					id;
	int					nbp;
	std::string			name;
	std::string			mapName;
	std::stringstream	ss;
	unsigned long		f;
	int					i = 0;

	if (this->_socket->getStatus() == CONNECTED)
	{
		this->clearList();
		data = this->_sc.get_rooms();
		this->_socket->sendData(data);
		this->_socket->receiveData(data);
		if (data.size() >= 7)
		{
			ss << data;
			ss >> type;
			data = data.substr(3);
			ss.clear();
			ss.str("");
			ss << data;
			ss >> size;
			if (type == 1 && size != 0)
			{
				this->_ids.resize(size, 0);
				data = data.substr(4);
				while (!data.empty())
				{
					ss.clear();
					ss.str("");
					ss << data.substr(0, 2);
					ss >> id;
					ss.clear();
					ss.str("");
					ss << data[3];
					ss >> nbp;
					data = data.substr(5);
					if ((f = data.find_first_of(';')) == std::string::npos)
						break ;
					name = data.substr(0, f);
					data = data.substr(10);
					if ((f = data.find_first_of(';')) == std::string::npos)
						break ;
					mapName = data.substr(0, f);
					data = data.substr(10);
					ss.clear();
					ss.str("");
					this->_ids[i] = id;
					ss << "#" << std::setfill('0') << std::setw(2) <<  id << "   " << name <<  "   Map: " << mapName << "  " << nbp << "/4";
					this->_buttons.push_back(new Button(50, 150 + (i * 40), 0, 0, 20, ss.str(), "", NONE));
					this->_buttons.push_back(new Button(500, 150 + (i * 40), 40, 30, 20, "Join", "", GAMEPLAY));
					i++;
				}
			}
		}
	}
}

void								MenuListState::clearList()
{
	std::list<Button *>::iterator	it;

	while (true)
	{
		it = this->_buttons.end();
		--it;
		if ((*it)->getState() == NONE || (*it)->getState() == GAMEPLAY)
		{
			this->_buttons.erase(it);
		}
		else
			break ;
	}
}

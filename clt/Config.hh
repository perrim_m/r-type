//
//  Config.h
//  R-Type
//
//  Created by Stern on 01/05/2014.
//
//

#ifndef CONFIG_HH_
# define CONFIG_HH_

# include <string>
# include <fstream>
# include <stdexcept>

class Config
{
private:
	std::fstream	_fs;

public:
	Config(const std::string &);
	virtual ~Config();

	bool	getInfo(const std::string &, std::string &);
	void	setInfo(const std::string &, const std::string &);
};

#endif // !CONFIG_HH_

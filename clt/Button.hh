//
//  Button.hh
//  R-Type
//
//  Created by Stern on 01/05/2014.
//
//

#ifndef BUTTON_HH_
# define BUTTON_HH_

# include <string>

# include "Client.hh"

class Button
{
private:
	float				_x;
	float				_y;
	float				_width;
	float				_height;
	unsigned int		_size;
	const std::string	_texture;
	const std::string	_text;
	eState				_state;

public:
	Button(float, float, float, float, unsigned int, const std::string &, const std::string &, eState);
	virtual ~Button();

	float				getX() const;
	float				getY() const;
	float				getWidth() const;
	float				getHeight() const;
	unsigned int		getSize() const;
	const std::string	&getTexture() const;
	const std::string	&getText() const;
	eState				getState() const;
	bool				ifCollision(float, float) const;
};

#endif // !BUTTON_HH_
